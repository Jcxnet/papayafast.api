Hola {{$userName}},
esta es la confirmación de tu pedido que será enviado a:

{{$address}}

Este es el detalle de tu pedido:
Pedido {{$numeroPedido}} - {{$fechaHora}}
----------------------------------------------------------
@foreach($products as $product)
<?php
	$name = str_replace('<strong>','',"({$product['cantidad']}) {$product['name']}");
	$name = str_replace('</strong>','',$name);
	$name = explode('<br>-',$name);
?>
@foreach($name as $item)
{{ $item }}
@endforeach
S/. {{ sprintf("%0.2f",$product['price'] * $product['cantidad']) }}
----------------------------------------------------------
@endforeach
Subtotal        : S/. {{ sprintf("%0.2f",$pagos['subtotal']) }}
Comisión online : S/. {{ sprintf("%0.2f",$pagos['comision']) }}
----------------------------------------------------------
Total           S/. {{ sprintf("%0.2f",$pagos['total']) }}


Gracias por tu compra,
{{ env('APP_COMMERCE_NAME') }}

