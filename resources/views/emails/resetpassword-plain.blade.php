Hola <strong>{{$name}}</strong>,
has solicitado cambiar tu clave.,<br>
El código para que puedas cambiar la clave es el siguiente:

<strong>{{$code}}</strong>

Si no has solicitado el cambio de tu clave, por favor, ignora este mensaje o responde para informarnos. Este código para cambiar de clave sólo es válido para los próximos 30 minutos.

Gracias.

P.D. También nos gustaría saber si podemos ayudarte con cualquier problema que tengas. Por favor, responde a este mensaje si deseas hacer alguna pregunta o simplemente decir hola.