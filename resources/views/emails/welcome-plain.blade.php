Bienvenido <strong>{{$name}}</strong>,<br/>
gracias por registrarte, estamos muy contentos de tenerte con nosostros.
<br/>
Recuerda que debes verificar tu cuenta:
<a href="{{ $urlVerify }}">Verificar mi cuenta</a>

Para que puedas inciar sesión debes utilizar la cuenta de email <strong>{{ $email }}</strong>  y la clave que ingresaste.
Gracias.

P.D. ¿Necesitas ayuda para empezar?. Por favor, responde a este mensaje si deseas hacer alguna pregunta.