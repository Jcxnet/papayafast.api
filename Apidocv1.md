

# Api

# Appinfo
# Información de la aplicación asociada a cada comercio

## Busca y compara la versión de la aplicación [POST /version]


+ Parameters
    + apikey: (string, required) - clave de la aplicación
    + version: (string, required) - versión de la aplicación enviada para comparar

+ Request (application/json)
    + Headers

            Accept: application/vnd.papayafast.v1+json
            Content-Type: application/json
    + Body

            {
                "apikey": "47cd76e43f74bbc2e1baaf194d07e1fa",
                "version": "1.0.0"
            }

+ Response 202 (application/json)
    + Body

            {
                "error": "No se encontró información de la aplicación"
            }

+ Response 202 (application/json)
    + Body

            {
                "error": "Exception"
            }

+ Response 200 (application/json)
    + Body

            {
                "meta": {
                    "status": "update"
                },
                "data": {
                    "name": "Appinfo:class->name",
                    "version": "Appinfo:class->version",
                    "comercio_id": "Appinfo::class->comercio_id"
                }
            }

+ Response 200 (application/json)
    + Body

            {
                "meta": {
                    "status": "nochange"
                },
                "data": {
                    "name": "Appinfo:class->name",
                    "version": "Appinfo:class->version",
                    "comercio_id": "Appinfo::class->comercio_id"
                }
            }

## Busca y retorna la información de la aplicación [POST /verify]


+ Parameters
    + apikey: (string, required) - clave de la aplicación

+ Request (application/json)
    + Headers

            Accept: application/vnd.papayafast.v1+json
            Content-Type: application/json
    + Body

            {
                "apikey": "47cd76e43f74bbc2e1baaf194d07e1fa"
            }

+ Response 202 (application/json)
    + Body

            {
                "error": "No se encontró la aplicación"
            }

+ Response 202 (application/json)
    + Body

            {
                "error": "Exception"
            }

+ Response 200 (application/json)
    + Body

            {
                "meta": {
                    "status": "ok"
                },
                "data": {
                    "name": "Appinfo:class->name",
                    "version": "Appinfo:class->version",
                    "comercio_id": "Appinfo::class->comercio_id"
                }
            }

# AppApiv1ControllersUsersController

# AppApiv1ControllersComerciosController

# AppApiv1ControllersLocalsController

# AppApiv1ControllersMenusController

# AppApiv1ControllersNostocksController

# Addresses [/addresses]
# Gestión de Direcciones

## Inserta una dirección del usuario en la base de datos [POST /addresses/create]


+ Parameters
    + address: (string, required) - Dirección exacta
    + name: (string, required) - Nombre que asigna el usuario a la dirección
    + phone: (integer, required) - Número de teléfono asociado a la dirección
    + lat: (float, required) - Valor de la coordenada de Latitud de la dirección
    + lng: (float, required) - Valor de la coordenada de Longitud de la dirección
    + local_id: (integer, required) - Identificador del local que cubre la dirección

+ Request (application/json)
    + Headers

            Accept: application/vnd.papayafast.v1+json
            Authorization Bearer: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ
            Content-Type: application/json
    + Body

            {
                "address": "dirección",
                "name": "nombre de la dirección",
                "phone": "123456",
                "lat": "-12.4465",
                "lng": "-72.6545",
                "local_id": "2"
            }

+ Response 202 (application/json)
    + Body

            {
                "meta": {
                    "status": "error"
                },
                "data": {
                    "message": "Usuario no autorizado"
                }
            }

+ Response 202 (application/json)
    + Body

            {
                "meta": {
                    "status": "error"
                },
                "data": {
                    "message": "No se encontró el local asociado a la dirección"
                }
            }

+ Response 202 (application/json)
    + Body

            {
                "meta": {
                    "status": "error"
                },
                "data": {
                    "message": "No se pudo crear la dirección"
                }
            }

+ Response 202 (application/json)
    + Body

            {
                "meta": {
                    "status": "error"
                },
                "data": {
                    "message": "JWTException::class"
                }
            }

+ Response 200 (application/json)
    + Body

            {
                "meta": {
                    "status": "ok"
                },
                "data": {
                    "id": "Address::class->uuid",
                    "name": "Address::class->name",
                    "address": "Address::class->address",
                    "phone": "Address::class->phone",
                    "zone": "Address::class->zone",
                    "point": {
                        "lat": "Address::class->lat",
                        "lng": "Address::class->lng"
                    },
                    "selected": "Address::class->selected",
                    "indatabase": true
                }
            }

## Busca y retorna la info de una dirección por su uuid [GET /addresses/info]


+ Parameters
    + id: (string, required) - uuid de la dirección

+ Request (application/json)
    + Headers

            Accept: application/vnd.papayafast.v1+json
            Authorization Bearer: token
            Content-Type: application/json
    + Body

            {
                "id": "6ee6b8f2-cb31-4f9e-a949-edacdeee8ec6"
            }

+ Response 202 (application/json)
    + Body

            {
                "error": "Usuario no autorizado"
            }

+ Response 202 (application/json)
    + Body

            {
                "error": "No se encontró la dirección"
            }

+ Response 202 (application/json)
    + Body

            {
                "error": "JWTException::class"
            }

+ Response 200 (application/json)
    + Body

            {
                "meta": {
                    "status": "ok"
                },
                "data": {
                    "id": "Address::class->uuid",
                    "name": "Address::class->name",
                    "address": "Address::class->address",
                    "phone": "Address::class->phone",
                    "zone": "Address::class->zone",
                    "point": {
                        "lat": "Address::class->lat",
                        "lng": "Address::class->lng"
                    },
                    "selected": "Address::class->selected",
                    "indatabase": true
                }
            }

## Retorna una colección con las direciones de un usuario [GET /addresses/user]


+ Request (application/json)
    + Headers

            Accept: application/vnd.papayafast.v1+json
            Authorization Bearer: token
            Content-Type: application/json

+ Response 202 (application/json)
    + Body

            {
                "error": "No se encontraron direcciones"
            }

+ Response 202 (application/json)
    + Body

            {
                "error": "JWTException::class"
            }

+ Response 200 (application/json)
    + Body

            {
                "meta": {
                    "status": "ok"
                },
                "data": [
                    {
                        "id": "Address::class->uuid",
                        "name": "Address::class->name",
                        "address": "Address::class->address",
                        "phone": "Address::class->phone",
                        "zone": "Address::class->zone",
                        "point": {
                            "lat": "Address::class->lat",
                            "lng": "Address::class->lng"
                        },
                        "selected": "Address::class->selected",
                        "indatabase": true
                    }
                ]
            }

## Elimina una dirección del usuario [DELETE /addresses/remove]


+ Parameters
    + id: (string, required) - uuid de la dirección

+ Request (application/json)
    + Headers

            Accept: application/vnd.papayafast.v1+json
            Authorization Bearer: token
            Content-Type: application/json
    + Body

            {
                "id": "6ee6b8f2-cb31-4f9e-a949-edacdeee8ec6"
            }

+ Response 202 (application/json)
    + Body

            {
                "error": "Usuario no autorizado"
            }

+ Response 202 (application/json)
    + Body

            {
                "error": "No se encontró la dirección"
            }

+ Response 202 (application/json)
    + Body

            {
                "error": "JWTException::class"
            }

+ Response 200 (application/json)
    + Body

            {
                "meta": {
                    "status": "ok"
                },
                "data": []
            }

## Actualiza los datos de una dirección del usuario [PUT /addresses/update]


+ Parameters
    + id: (string, required) - uuid de la dirección
    + address: (string, required) - Dirección exacta
    + name: (string, required) - Nombre que asigna el usuario a la dirección
    + phone: (integer, required) - Número de teléfono asociado a la dirección
    + lat: (float, required) - Valor de la coordenada de Latitud de la dirección
    + lng: (float, required) - Valor de la coordenada de Longitud de la dirección
    + local_id: (integre, required) - Identificador del local que cubre la dirección

+ Request (application/json)
    + Headers

            Accept: application/vnd.papayafast.v1+json
            Authorization Bearer: token
            Content-Type: application/json
    + Body

            {
                "id": "6ee6b8f2-cb31-4f9e-a949-edacdeee8ec6",
                "address": "dirección",
                "name": "nombre de la dirección",
                "phone": "123456",
                "lat": "-12.4465",
                "lng": "-72.6545",
                "local_id": "2"
            }

+ Response 202 (application/json)
    + Body

            {
                "error": "Usuario no autorizado"
            }

+ Response 202 (application/json)
    + Body

            {
                "error": "No se encontró el local asociado a la dirección"
            }

+ Response 202 (application/json)
    + Body

            {
                "error": "No se encontró la dirección"
            }

+ Response 202 (application/json)
    + Body

            {
                "error": "No se pudo actualizar la dirección"
            }

+ Response 202 (application/json)
    + Body

            {
                "error": "JWTException::class"
            }

+ Response 200 (application/json)
    + Body

            {
                "meta": {
                    "status": "ok"
                },
                "data": {
                    "id": "Address::class->uuid",
                    "name": "Address::class->name",
                    "address": "Address::class->address",
                    "phone": "Address::class->phone",
                    "zone": "Address::class->zone",
                    "point": {
                        "lat": "Address::class->lat",
                        "lng": "Address::class->lng"
                    },
                    "selected": "Address::class->selected",
                    "indatabase": true
                }
            }

# Checkout [/checkouts]
# Validación de los métodos de pago

## Verifica el método de pago seleccionado por el usuario [POST /checkouts/validate]


+ Parameters
    + type: (string, required) - *cash*:pago en efectivo,*pos*:pago con POS,*credit*:pago con nueva tarjeta de crédito,*cclist*:pago con tarjeta de crédito almacenada
    + total: (float, required) - Total a pagar
    + card: (string, required) - *visa*,*visa electron*,*mastercard* tipo de tarjeta para pago con POS
    + inputid: (string, required) - Identificador del elemento del formulario
    + amount: (float, required) - Monto con el que el usuario cancela el pedido en efectivo
    + nameoncard: (string, required) - Nombre del titular de la tarjeta de crédito
    + numbercard: (integer, required) - Número de la tarjeta de crédito
    + expirecard: (date, required) - Fecha de expiración de la tarjeta MM/YYYY
    + securecard: (integer, required) - Código CCV de la tarjeta de crédito
    + billing: (array, required) - Información de los datos de facturación
    + billing['type']: (string, required) - *boleta*,*factura* Tipo de facturación
    + billing['inputid']: (string, required) - Identificador del elemento del formulario
    + billing['name']: (string, required) - Nombre para la facturación
    + billing['ruc']: (integer, required) - Número de RUC para la factura
    + billing['razon']: (string, required) - Nombre de la empresa para la factura
    + billing['address']: (string, required) - Dirección de la empresa para la factura

+ Request (application/json)
    + Headers

            Accept: application/vnd.papayafast.v1+json
            Authorization Bearer: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ
            Content-Type: application/json
    + Body

            {
                "type": "cash",
                "total": "60.00",
                "amount": "100.00",
                "inputid": "txt_total",
                "billing": {
                    "type": "boleta",
                    "name": "John Doe",
                    "inputid": "txt_boleta"
                }
            }

+ Response 202 (application/json)
    + Body

            {
                "meta": {
                    "status": "error"
                },
                "data": {
                    "message": "Usuario no autorizado"
                }
            }

+ Response 202 (application/json)
    + Body

            {
                "meta": {
                    "status": "error"
                },
                "data": {
                    "message": "No hemos podido verificar el estado del pedido, intenta nuevamente."
                }
            }

+ Response 202 (application/json)
    + Body

            {
                "meta": {
                    "status": "error"
                },
                "data": {
                    "message": "No se pudo generar el método de validación, intente nuevamente"
                }
            }

+ Response 202 (application/json)
    + Body

            {
                "meta": {
                    "status": "error"
                },
                "data": {
                    "errors": {
                        "id": "txt_total",
                        "msg": "Solo debe ingresar números"
                    }
                }
            }

+ Response 202 (application/json)
    + Body

            {
                "meta": {
                    "status": "error"
                },
                "data": {
                    "errors": {
                        "id": "txt_total",
                        "msg": "El monto de pago debe ser mayor al total del pedido"
                    }
                }
            }

+ Response 202 (application/json)
    + Body

            {
                "meta": {
                    "status": "error"
                },
                "data": {
                    "message": "JWTException"
                }
            }

+ Response 200 (application/json)
    + Body

            {
                "meta": {
                    "status": "ok"
                },
                "data": {
                    "payment": "6affdae3b3c1aa6aa7689e9b6a7b3225a636aa1ac0025f490cca1285ceaf1487"
                }
            }

+ Request (application/json)
    + Headers

            Accept: application/vnd.papayafast.v1+json
            Authorization Bearer: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ
            Content-Type: application/json
    + Body

            {
                "type": "cash",
                "total": "60.00",
                "amount": "100.00",
                "inputid": "txt_total",
                "billing": {
                    "type": "factura",
                    "name": "John Doe",
                    "ruc": "12345678901",
                    "razon": "Empresa",
                    "address": "dirección empresa"
                }
            }

+ Response 202 (application/json)
    + Body

            {
                "meta": {
                    "status": "error"
                },
                "data": {
                    "message": "Usuario no autorizado"
                }
            }

+ Response 202 (application/json)
    + Body

            {
                "meta": {
                    "status": "error"
                },
                "data": {
                    "errors": {
                        "id": "txt_total",
                        "msg": "Solo debe ingresar números"
                    }
                }
            }

+ Response 202 (application/json)
    + Body

            {
                "meta": {
                    "status": "error"
                },
                "data": {
                    "errors": {
                        "id": "txt_total",
                        "msg": "El monto de pago debe ser mayor al total del pedido"
                    }
                }
            }

+ Response 200 (application/json)
    + Body

            {
                "meta": {
                    "status": "ok"
                },
                "data": {
                    "payment": "6affdae3b3c1aa6aa7689e9b6a7b3225a636aa1ac0025f490cca1285ceaf1487"
                }
            }

## Retorna el id del local y la zona de acuerdo a la orden actual [POST /checkouts/local]


+ Parameters
    + data: (json, required) - información encriptada del checkout de la orden actual

+ Request (application/json)
    + Headers

            Accept: application/vnd.papayafast.v1+json
            Authorization Bearer: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ
            Content-Type: application/json
    + Body

            {
                "data": {
                    "payment": "6affdae3b3c1aa6aa7689e9b6a7b3225a636aa1ac0025f490cca1285ceaf1487"
                }
            }

+ Response 202 (application/json)
    + Body

            {
                "meta": {
                    "status": "error"
                },
                "data": {
                    "message": "Usuario no autorizado"
                }
            }

+ Response 202 (application/json)
    + Body

            {
                "meta": {
                    "status": "error"
                },
                "data": {
                    "message": "No se encontró la orden para enviar el pedido"
                }
            }

+ Response 202 (application/json)
    + Body

            {
                "meta": {
                    "status": "error"
                },
                "data": {
                    "message": "JWTException"
                }
            }

+ Response 200 (application/json)
    + Body

            {
                "meta": {
                    "status": "ok"
                },
                "data": {
                    "id": "549048e1-52e9-46e9-ba39-3a9a5ca7b076",
                    "zone": "green"
                }
            }

+ Response 200 (application/json)
    + Body

            {
                "meta": {
                    "status": "ok"
                },
                "data": {
                    "id": "549048e1-52e9-46e9-ba39-3a9a5ca7b076",
                    "zone": "orange"
                }
            }

# AppApiv1ControllersPaymentsController

# AppApiv1ControllersOrdersController

# AppApiv1ControllersCreditcardsController