<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Dingo\Api\Routing\Helpers;
use Dingo\Api\Exception\ValidationHttpException;

use App\User;
use App\Data;
use App\Appinfo;

use Mail;
use Crypt;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;
    use Helpers;

    /**
     * envía las peticiones a la API vía CURL
     * @param  string $url
     * @param  array  $data
     * @param  array  $header
     * @param  string $request
     * @return json
     */
    public function sendData($url=null,$data=[],$header=[],$request = "POST"){

    	if(is_null($url))
    		return ['meta'=>['status'=>'error','message'=>'URL no proporcionada']];

   		$header = array_merge($header,['Accept:application/vnd.papayafastauth.v1+json','Content-Type:application/json','Cache-Control:no-cache']);

			$curl = curl_init();
			curl_setopt_array($curl, [
			  CURLOPT_URL => $url,
			  CURLOPT_RETURNTRANSFER 	=> true,
			  CURLOPT_ENCODING 				=> "utf-8",
			  CURLOPT_MAXREDIRS 			=> 10,
			  CURLOPT_TIMEOUT 				=> 30,
			  CURLOPT_HTTP_VERSION 		=> CURL_HTTP_VERSION_1_1,
			  CURLOPT_SSL_VERIFYPEER 	=> false, //be careful ->add ssl certificate
			  CURLOPT_CUSTOMREQUEST 	=> $request,
			  CURLOPT_POSTFIELDS 			=> json_encode($data),
			  CURLOPT_HTTPHEADER 			=> $header,
			  CURLOPT_COOKIESESSION 	=> false,
			]);

			$response = json_decode(curl_exec($curl), true);
			$err = curl_error($curl);
			curl_close($curl);

      if ($err) {
			  return ['meta'=>['status'=>'error','message'=>"cURL Error #: $err"]];
			} else {
			  return $response;
			}
    }

    /**
     * crea una instancia de la clase Data
     * @param Data
     */
    public function setData(array $data){
    	$item = new Data;
    	foreach ($data as $key => $value) {
    		$item[$key] = $value;
    	}
    	return $item;
    }

    /**
     * envía un email
     * @param  string $template
     * @param  array  $emailData
     * @param  array  $data
     * @return boolean
     */
    public function sendEmail($template, $emailData, $data){
    	try {
    	 	Mail::send($template, $emailData, function ($message) use ($data) {
            $message->from($data['fromEmail'],$data['fromName']);
            $message->to($data['toEmail'], $data['toName']);
            $message->subject($data['subject']);
        	});
    	}catch(Exception $e){
    		return false;
    	}
    	return true;
    }

    /**
     * obtienen el ID de un usuario según su UUID
     * @param  string $uuid
     * @return mixed
     */
    public function getIdFromUuid($uuid){
    	if($user = User::select('id')->where('uuid',$uuid)->first())
    		return $user->id;
    	return false;
    }

    /**
     * genera un UUID
     * @return string
     */
    public function generateUuid(){
    	$faker = \Faker\Factory::create();
    	return $faker->uuid();
    }

    /**
     * agrega el header necesario para que se verifique el token de JWT
     * @param  string $token
     * @return array
     */
    public function createTokenHeader($token=false){
       $header = $token;
       if($token){
       	$token = $this->decryptData($token);
       	$header = ["Authorization: Bearer $token"];
       }
       return $header;
    }

    /**
     * extrae el valor de key almacenado en el token JWT
     * @param  string $token
     * @param  string $key
     * @return string
     */
    public function getKeyToken($token,$key){
    	return $this->getTokenValue($token,$key);
    }

    /**
     * extrae el valor de key almacenado en el token JWT
     * @param  string $token
     * @param  string $key
     * @return string
     */
    protected function getTokenValue($token,$key){
      try{
        list($header,$payload,$sign) = explode('.',$token);
        $payload = json_decode(base64_decode($payload),true);
        return $payload[$key];
      }catch(Exception $e){
        return false;
      }
    }

  /**
   * calcula el valor crc32 de una cadena
   * @param  mixed  $data
   * @return integer
   */
  public function calculateChecksum($data){
  	if(is_array($data))
  		$data = json_encode($data);
  	if(is_string($data))
  		return sprintf("%u",crc32($data));
  	return 0;
  }

   /**
   * encripta data
   * @param  string $data
   * @return string
   */
  public function encryptData($data){
    if(is_array($data))
      $data = json_encode($data);
    if(!is_string($data))
    	$data = (string)$data;
    if(is_string($data))
      return Crypt::encrypt($data);
    return false;
  }

  /**
   * desencripta data
   * @param  string $data
   * @return string
   */
  public function decryptData($data){
    try {
     return Crypt::decrypt($data);
    } catch (DecryptException $e) {
      return false;
    }
  }

  /**
   * agrega un mensaje al log del registro
   * @param  array 		$log
   * @param  string 	$message
   * @return array
   */
  public function dataLog($log,$message,$ip){
    if(is_array($log))
      $data = $log;
    else
      $data = [];
    $datetime = date('Y-m-j H:i:s');
    $data[] = ['datetime'=>$datetime,'ip'=>$ip, 'message'=>$message];
    return $data;
  }

  public function maskEmail( $email ) {
  	return $email;
    $char_shown = 2;
    $mail_parts = explode("@", $email);
    $username = $mail_parts[0];
    $len = strlen( $username );
    if( $len <= $char_shown ){
      return implode("@", $mail_parts );
    }
    $mail_parts[0] = substr($username,0,$char_shown).str_repeat("*",$len-$char_shown-1).substr($username,$len-$char_shown+($char_shown-1),1);
 		return implode("@", $mail_parts );
  }

     /**
   * envía el email de confirmación del pedido generado
   * @param  integer  $orderNumber
   * @param  User     $user
   * @param  string   $fechaHora
   * @return json
   */
  public function sendEmailOrder($order,$user,$fechaHora){
    $products = $this->productsEmail($order->products);
    $pagos    = $this->calculatePayments($products);

    $emailData = [
        'numeroPedido'  =>  $order->number,
        'fechaHora'     =>  $fechaHora,
        'userName'      =>  $user['name'],
        'address'       =>  $order->address['address'],
        'products'      =>  $products,
        'pagos'         =>  $pagos
      ];

    $data = [
    		'fromEmail' =>	env('MAIL_SYSTEM_EMAIL'),
    		'fromName' 	=>	env('MAIL_SYSTEM_NAME'),
    		'toEmail' 	=>	$user['email'],
    		'toName' 		=>	$user['name'],
    		'subject' 	=> 	"Tu pedido ha sido recibido"
    	];

    $res = $this->sendEmail(['emails.order','emails.order-plain'],$emailData,$data);
    return json_encode($emailData);
  }

  /**
   * retorna los datos necesarios de cada producto para enviarlos por email
   * @param  array  $list
   * @return array
   */
  private function productsEmail($list){
    $products = [];

    foreach ($list as $item) {
      $product = [];
      if (count($item['image']) == 0)
        $product['image'] = asset('img/0.jpg');
      else
        $product['image'] = $item['image'][0];

      $product['price']    =  $this->productPrice($item);
      $product['cantidad'] =  $item['cantidad'];
      $product['name']     = "<strong>{$item['title']}</strong>";
      $product['deleted']  = false;
      foreach ($item['groups'] as $group) {
        foreach ($group['properties'] as $property) {
          if($property['order'] == 1){
            $product['name'] .= '<br>- '.$property['title'];
            break;
          }
        }
      }
      foreach ($item['properties'] as $property) {
        //if($property['order'] == 1){
          $product['name'] .= '<br>- '.$property['title'];
        /*  break;
        }*/
      }
      $products[] = $product;
    }

    return $products;
  }

  public function calculatePayments($products){
  	$subtotal = 0; $comision = 0;
    foreach ($products  as $product) {
      $subtotal += $this->productPrice($product) * $product['cantidad'];
    }
    if($subtotal != 0){
      $comision  = $this->calculateComission($subtotal);
    }
  	return ['subtotal' => $subtotal, 'comision'=> $comision, 'total' => $subtotal+$comision];
  }

  private function productPrice($product){
  	$price = (float)$product['price'];
  	if(array_key_exists('properties',$product)){
  		if($product['properties']){
  			$properties = is_array($product['properties'])?$product['properties']:json_decode($product['properties'],true);
    		foreach ($properties as $property) {
    				$price += (float)$property['price'];
    		}
  		}
  	}
  	if(array_key_exists('groups',$product)){
  		if($product['groups']){
  			$groups = is_array($product['groups'])?$product['groups']:json_decode($product['groups'],true);
    		foreach ($groups as $group) {
    			foreach ($group['properties'] as $property) {
    				if($property['order'] == 1){
    					$price += (float)$property['price'];
    					break;
    				}
    			}
    		}
  		}
  	}
  	return (float)$price;
  }

 	private function calculateComission($total){
    $commission = [20 => 1,50 => 1.5, 75 => 2, 100 => 2.5];
    foreach ($commission as $max => $amount) {
      if($total <= $max)
        return $amount;
    }
    return 3;
  }
}
