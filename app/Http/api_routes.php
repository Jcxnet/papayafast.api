<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {

	$api->post('version', ['as' => 'app.version', 'uses' => 'App\Api\v1\Controllers\AppinfoController@version']);
	$api->post('verify', 	['as' => 'app.verify.token', 'uses' => 'App\Api\v1\Controllers\AppinfoController@getByToken']);

	// Users routes
	$api->group(['prefix'=>'users'],function($api){
		$api->post('login', 		['as' => 'user.login',		'uses' => 'App\Api\v1\Controllers\UsersController@login'] );
		$api->post('register', 	['as' => 'user.register',	'uses' => 'App\Api\v1\Controllers\UsersController@register'] );
		$api->post('verify', 		['as' => 'user.verify',		'uses' => 'App\Api\v1\Controllers\UsersController@verify'] );
		$api->post('recovery', 	['as' => 'user.recovery',	'uses' => 'App\Api\v1\Controllers\UsersController@recovery'] );
		$api->post('reset', 		['as' => 'user.reset',		'uses' => 'App\Api\v1\Controllers\UsersController@reset'] );
		$api->group(['middleware'=>'api.auth'],function($api){
			$api->post('update', 		['as' => 'user.update',	'uses' => 'App\Api\v1\Controllers\UsersController@update'] );
			$api->post('logout', 		['as' => 'user.logout',	'uses' => 'App\Api\v1\Controllers\UsersController@logout'] );
			$api->post('info', 			['as' => 'user.info',		'uses' => 'App\Api\v1\Controllers\UsersController@info'] );
			$api->post('locals', 		['as' => 'user.locals',	'uses' => 'App\Api\v1\Controllers\UsersController@locals'] );
			//$api->post('startapp',	['as' => 'user.start',		'uses' => 'App\Api\v1\Controllers\UsersController@initialAppData'] );
		});
	});

	// Comercios routes
	$api->group(['prefix'=>'comercios'],function($api){
		$api->post('info', 		['as' => 'comercios.info',		'uses' => 'App\Api\v1\Controllers\ComerciosController@info'] );
	});


	// Locals routes
	$api->group(['prefix'=>'locals'],function($api){
		$api->post('/', 						['as' => 'locals.all',						'uses' => 'App\Api\v1\Controllers\LocalsController@all'] );
		$api->post('address/cover',	['as' => 'locals.address.cover',	'uses' => 'App\Api\v1\Controllers\LocalsController@coverPoint'] );
		$api->post('status',				['as' => 'locals.status',					'uses' => 'App\Api\v1\Controllers\LocalsController@status'] );
		$api->post('open',					['as' => 'locals.open',						'uses' => 'App\Api\v1\Controllers\LocalsController@open'] );
		$api->post('delivery',			['as' => 'locals.delivery',				'uses' => 'App\Api\v1\Controllers\LocalsController@delivery'] );
		$api->group(['middleware'=>'api.auth'],function($api){
			$api->post('user/favorite',	['as' => 'locals.user.favorite',	'uses' => 'App\Api\v1\Controllers\LocalsController@favorite'] );
			$api->post('user/favorites',['as' => 'locals.user.favorites',	'uses' => 'App\Api\v1\Controllers\LocalsController@userlocals'] );
		});
	});

	// Menus routes
	$api->group(['prefix'=>'menus'],function($api){
		$api->post('/', 									['as' => 'menus.all',		'uses' => 'App\Api\v1\Controllers\MenusController@all'] );
		$api->post('products/available', 	['as' => 'menus.products.available',	'uses' => 'App\Api\v1\Controllers\NostocksController@products'] );
		$api->post('product/available', 	['as' => 'menus.product.available',		'uses' => 'App\Api\v1\Controllers\NostocksController@product'] );
	});

	// Addresses routes
	$api->group(['prefix'=>'addresses'],function($api){
		$api->group(['middleware'=>'api.auth'],function($api){
			$api->post	('create', 	['as' => 'address.create',			'uses' => 'App\Api\v1\Controllers\AddressesController@create'] );
			$api->delete('remove', 	['as' => 'address.remove',			'uses' => 'App\Api\v1\Controllers\AddressesController@remove'] );
			$api->put 	('update', 	['as' => 'address.update',			'uses' => 'App\Api\v1\Controllers\AddressesController@update'] );
			$api->post	('cover', 	['as' => 'address.local.cover',	'uses' => 'App\Api\v1\Controllers\AddressesController@localCover'] );
			$api->get 	('info', 		['as' => 'address.info',				'uses' => 'App\Api\v1\Controllers\AddressesController@getAddress'] );
			$api->get 	('user', 		['as' => 'address.user',				'uses' => 'App\Api\v1\Controllers\AddressesController@myAddresses'] );
			//$api->get 	('user/favorites', ['as' => 'address.user.favorites',	'uses' => 'App\Api\v1\Controllers\AddressesController@myFavorites'] );
			//$api->post 	('users/favorites/add', ['as' => 'address.user.favorites.add',	'uses' => 'App\Api\v1\Controllers\AddressesController@favoritesUserAdd'] );
		});
	});

	// Checkouts routes
	$api->group(['prefix'=>'checkouts'],function($api){
		$api->group(['middleware'=>'api.auth'],function($api){
			$api->post('validate', 	['as' => 'checkouts.validate',	'uses' => 'App\Api\v1\Controllers\CheckoutsController@validateMethod'] );
			$api->post('local', 		['as' => 'checkouts.local',			'uses' => 'App\Api\v1\Controllers\CheckoutsController@local'] );
		});
	});
	// Payments routes
	$api->group(['prefix'=>'payments'],function($api){
		$api->group(['middleware'=>'api.auth'],function($api){
			$api->post('generate', 		['as' => 'payments.generate',		'uses' => 'App\Api\v1\Controllers\PaymentsController@generatePayment'] );
		});
	});

	// Orders routes
	$api->group(['prefix'=>'orders'],function($api){
		$api->post('status/commerce',			['as' => 'orders.status.commerce',			'uses' => 'App\Api\v1\Controllers\OrdersController@statusByCommerce'] );
		$api->get('status/commerce/all',	['as' => 'orders.status.commerce.all',	'uses' => 'App\Api\v1\Controllers\OrdersController@statusAllCommerce'] );
		$api->group(['middleware'=>'api.auth'],function($api){
			$api->post('generate', 	['as' => 'orders.generate',	'uses' => 'App\Api\v1\Controllers\OrdersController@generateOrder'] );
			$api->post('send', 			['as' => 'orders.send',			'uses' => 'App\Api\v1\Controllers\OrdersController@sendOrder'] );
			$api->post('confirmation',		['as' => 'orders.confirmation',		'uses' => 'App\Api\v1\Controllers\OrdersController@confirmation'] );
			$api->get('history',		['as' => 'orders.history',		'uses' => 'App\Api\v1\Controllers\OrdersController@history'] );
			$api->get('detail',		['as' => 'orders.detail',		'uses' => 'App\Api\v1\Controllers\OrdersController@detail'] );
			$api->post('status',		['as' => 'orders.status',		'uses' => 'App\Api\v1\Controllers\OrdersController@status'] );
		});
	});

	// Credit cards routes
	$api->group(['prefix'=>'creditcards'],function($api){
		$api->post('charge', 	['as' => 'creditcards.charge',	'uses' => 'App\Api\v1\Controllers\CreditcardsController@process'] );
		$api->get('list', 		['as' => 'creditcards.user',		'uses' => 'App\Api\v1\Controllers\CreditcardsController@cards'] );
		$api->post('remove', 	['as' => 'creditcards.remove',	'uses' => 'App\Api\v1\Controllers\CreditcardsController@remove'] );
	});


});