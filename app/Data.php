<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Data extends Model
{
    protected $table = null;
    protected $fillable = ['*'];
    public $timestamps = false;
}
