<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table 			= "orders";
    public  	$timestamps	= false;
    protected $casts			=	[	'created' 	  => 'datetime',
    													'amount' 		  => 'array',
    													'address'  	  => 'array',
                              'orderformat' => 'array',
                              'payment' 	  => 'array',
    													'products'	  => 'array',
    													'log' 			  => 'array',
    													'deleted' 	  => 'boolean',
    													'subtotal' 	  => 'float',
    													'comision' 	  => 'float'];

    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function payment(){
    	return $this->belongsTo('App\Payment');
    }

    public function enumPaymentStatus($status){
      switch(strtoupper($status)){
        case 'NOPAGADO' : return 1;
        case 'PAGADO'   : return 2;
        case 'DEVUELTO' : return 3;
      }
      return NULL;
    }

    public function enumOrderStatus($status){
      switch(strtoupper($status)){
        case 'GENERADO'   		: return 1;
        case 'PENDIENTE'  		: return 2;
        case 'LOCAL'      		: return 3;
        case 'PRODUCCION'			: return 4;
        case 'ENVIADO'    		: return 5;
        case 'RECIBIDO'   		: return 6;
        case 'ERROR DATOS'		: return 7;
        case 'ERROR CONEXION'	: return 8;
        case 'ANULADO'    		: return 9;
        default: return 10;//'NO STATUS'
      }
      return NULL;
    }

}
