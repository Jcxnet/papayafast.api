<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Appinfo extends Model
{
    protected $table ='apps';
    protected $dates = ['deleted_at'];
    protected $casts = ['processors'=>'array'];

    public function comercio(){
        return $this->belongsTo('App\Comercio');
    }

    public function getProcessor($apikey=false,$key=false){
    	if(!$apikey || !$key)
    		return false;
    	if(!$appInfo = $this->where('token',$apikey)->first())
    		return false;
    	if(!array_key_exists($key,$appInfo->processors))
    		return false;
    	return $appInfo->processors[$key];
    }

    public function comercioId($apikey=false){
    	if(!$apikey)
    		return false;
    	if(!$appInfo = $this->where('token',$apikey)->first())
    		return false;
    	return $appInfo->comercio_id;
    }

}
