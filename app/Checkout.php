<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Checkout extends Model
{
    protected $table 			= "checkouts";
    public  	$timestamps	= false;
    protected $casts			=	[	'log'  => 'array' ];

    public function enumStatus($status){
      switch(strtoupper($status)){
        case 'CREATED'	: return 1;
        case 'VALIDATE'	: return 2;
        case 'PAYMENT'	: return 3;
        case 'ORDER'		: return 4;
        case 'CREDIT'		: return 5;
        case 'SEND'			: return 6;
        case 'COMPLETED': return 7;
      }
      return NULL;
    }

    /**
     * verifica si hay alguna transacción pendiente del usuario
     * @param  integer $userid
     * @return boolean
     */
    public function pendingUserCheckout($userid){
      $total = $this->where('user_id',$userid)->where('status','!=','COMPLETED')->count();
      return ($total > 0);
    }

    /**
     * retorna el id del proceso actual del usuario (si existe)
     * @param  integer  $userid
     * @return integer / false
     */
    public function getCheckout($userid){
      $checkout = $this->select('id')->where('user_id',$userid)->where('status','!=','COMPLETED')->first();
      if($checkout)
        return $checkout->id;
      return false;
    }

    /**
     * retorna un valor del proceso actual
     * @param  integer  $id
     * @param  string   $key
     * @return $key value
     */
    public function getCheckoutValue($id,$key){
      $checkout = $this->select($key)->where('id',$id)->first();
      if($checkout)
        return $checkout->$key;
      return false;
    }

    /**
     * crea un registro para iniciar la transacción de pago del pedido
     * @param  integer $userid
     * @return integer
     */
    public function generateCheckout($userid){
      $checkout = new Checkout;
      $checkout->user_id  = $userid;
      $checkout->status   = $this->enumStatus('VALIDATE'); //set next step
      $checkout->datetime = date('Y-m-d H:i:s');
      $checkout->save();
      return $checkout->id;
    }

    /**
     * compara el estado actual del proceso
     * @param  integer  $id
     * @param  string   $status
     * @return boolean
     */
    public function isCheckoutStatus($id,$status){
      $checkout = $this->select('status')->where('id',$id)->first();
      if($checkout)
        return ($checkout->status == strtoupper($status));
      return false;
    }

    /**
     * compara la data recibida del formulario con la almacenada inicialmente
     * @param  integer  $id
     * @param  array    $data
     * @return boolean
     */
    public function changeCheckoutData($id,$data){
      $checkout = $this->select('data')->where('id',$id)->first();
      return ($checkout->data != md5(json_encode($data)));
    }

    /**
     * actualiza los valores del proceso
     * @param   integer  $id
     * @param   array    $data
     * @return  void
     */
    public function setCheckoutUpdate($id,$data){
      $checkout = $this->where('id',$id)->update($data);
    }
}
