<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = "payments";
    public  	$timestamps	= false;
    protected $casts			=	[	'number' 	  => 'string',
    													'typecc'	  => 'string',
    													'log' 			=> 'array',
                              'deleted' 	=> 'boolean',
                              'logtransaction' => 'array',];

    /*public function order(){
    	return $this->hasMany('App\Order');
    }*/

    public function enumMethod($method){
      switch($method){
        case 'CREDIT': return 1; //enum CARD
        case 'CCLIST': return 1; //enum CARD
        case 'POS': return 2;
        case 'CASH': return 3;
      }
      return NULL;
    }
}
