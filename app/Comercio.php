<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comercio extends Model
{
	use SoftDeletes;

    protected $table = 'comercios';
    protected $dates = ['deleted_at'];

    public function appinfo(){
        return $this->hasMany('App\Appinfo');
    }

    public function local(){
        return $this->hasMany('App\Local');
    }

    public function menu(){
        return $this->hasMany('App\Menu');
    }
}
