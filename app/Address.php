<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Address extends Model
{
    use SoftDeletes;

    protected $table = 'addresses';

    protected $fillable = [
        'address', 'name', 'phone','zone', 'lat', 'lng', 'user_id', 'uuid', 'local_id','favorite','selected'
    ];

    protected $hidden = [
        'id','local_id', 'user_id'
    ];

    protected $dates = ['deleted_at'];

    protected $casts = ['favorite' => 'boolean','selected' => 'boolean'];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function local(){
   	  return $this->belongsTo('App\Local');
    }
}
