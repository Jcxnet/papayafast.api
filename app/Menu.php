<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Image;

class Menu extends Model
{
    use SoftDeletes;

    protected $table = 'menus';
    protected $casts = ['menu'=>'array'];
    protected $dates = ['deleted_at'];

    public function appinfo(){
        return $this->belongsTo('App\Comercio');
    }
 /**
  * atributo firstPage
  * @return integer
  */
 public function getFirstPageAttribute(){
    $menu = $this->getItemMenu('menu');
    return $menu['page'];
  }

  /**
   * atributo extras
   * @return array
   */
  public function getExtrasAttribute(){
    return $this->getItemMenu('extras');
  }

  /**
   * atributo sizes
   * @return array
   */
  public function getSizesAttribute(){
    return $this->getItemMenu('sizes');
  }

  /**
   * atributo properties
   * @return array
   */
  public function getPropertiesAttribute(){
    $props = $this->getItemMenu('properties');
    $list = [];
    foreach ($props as $prop) {
    	$list[$prop['id']] = $prop;
    }
    return $list;
  }

  /**
   * atributo grupos
   * @return array
   */
  public function getGroupsAttribute(){
  	return $this->getItemMenu('groups');
  }

  /**
   * atributo products
   * @return array
   */
  public function getProductsAttribute(){
    $image = new Image;
    $products =  $this->getItemMenu('products');
    $list = [];
    $properties = $this->properties;
    foreach ($products as $product) {
      $product['properties'] = $this->extractProductData($product['properties'],$properties);
      $groups = [];
      $product['properties'] = $this->extractProductGroups($product['properties'],$groups);
      $product['groups'] = $groups;
      $product['image'] = $image->getImages($product['image'],'products');
      $list[$product['id']] = $product;
    }
    return $list;
  }

  /**
   * atributo pages
   * @return array
   */
  public function getPagesAttribute(){
    $image = new Image;
    $pages = $this->getItemMenu('pages');
    $list = [];
    foreach ($pages as $page) {
      $page['image'] = $image->getImages($page['image'],'pages');
      $list[$page['id']] = $page;
    }
    return $list;
  }

  /**
   * atributo buttons
   * @return array
   */
  public function getButtonsAttribute(){
    $image = new Image;
    $buttons = $this->getItemMenu('buttons');
    $list = [];
    foreach ($buttons as $button) {
      $button['image'] = $image->getImages($button['image'],'buttons');
      $list[$button['id']] = $button;
    }
    return $list;
  }

  /**
   * extrae los grupos de la lista de propiedades de un producto
   * @param  array $properties
   * @param  array &$groups
   * @return array
   */
  protected function extractProductGroups($properties, &$groups){
    $props = []; $items = [];
    if(is_array($properties)){
      $props  = collect($properties)->where('group','')->sortBy('order')->toArray();
      $items  = collect($properties)->groupBy('group')->sortBy('grouporder')->sortBy('order')->filter(function($value,$key){
        return ($key != '');
      });
      foreach($items as $item){
        $group = [];
        $group['id']    = $item[0]['group'];
        $group['title'] = $item[0]['grouptitle'];
        $group['order'] = $item[0]['grouporder'];
        $group['mode']  = 'radio';
        $group['properties'] = [];
        foreach($item as $prop){
          $group['properties'][$prop['id']] = $prop;
        }
        $groups[$group['id']] = $group;
      }
    }
    return $props;
  }

  /**
   * extae las propiedades de un producto
   * @param  array 	$ids   [description]
   * @param  array  $items [description]
   * @param  boolean $sort  [description]
   * @return array
   */
  protected function extractProductData($ids, $items, $sort = true){
    if (is_array($ids)){
      if(count($ids)==0)
        return [];
    }
    if ($sort)
      return collect($items)->whereIn('id',$ids)->sortBy('order')->toArray();
    return collect($items)->whereIn('id',$ids)->toArray();
  }

  /**
   * obtiene la url de una imagen
   * @param  array $images
   * @return array
   */
	/*private function getImages($images){
    if(is_array($images)){
      if(count($images)>0){
        $images = Image::whereIn('id',$images)->get(['url'])->toArray();
        return collect($images)->flatten()->toArray();
      }
    }
    return [];
  }*/

  /**
   * extrae un item del array menu por la clave
   * @param  string $item
   * @return array
   */
  protected function getItemMenu($item){
  	$data = collect($this->menu)->pluck($item)->filter(function($value,$key){ return $value != NULL; })->values()->toArray();
  	if(count($data)>0)
    	return $data[0];
   	return [];
  }

}
