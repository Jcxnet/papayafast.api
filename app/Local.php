<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Image;

class Local extends Model
{
		use SoftDeletes;

	protected $table = 'locals';
	protected $casts = ['address'=>'array','contact'=>'array','zone'=>'array','timetable'=>'array','menu'=>'array','image'=>'array','delivery'=>'boolean'];
	protected $dates = ['deleted_at'];

	public function comercio(){
	  return $this->belongsTo('App\Comercio');
	}

	public function address(){
		return $this->hasMany('App\Address');
	}
	public function images(){
		$image = new Image;
		return $image->getImages($this->image,'locals');
	}

	public function zones(){
		$zones = $this->zone;
		if(is_array($zones)){
			$list = [];
			foreach ($zones as $zone) {
				$list[$zone['name']] = $zone['point'];
			}
			return $list;
		}
		return [];
	}

	public function openTime(){
	  $year = date('Y');
    $month = date('n');
    $day = date('w');
    $nday = date('j');
    $hour = date('G');
    $minute = date('i');

    $thistime = strtotime("$year-$month-$nday $hour:$minute:00");

    $local['timetable'] = $this->timetable;
    $local['open'] = false;

    if(count($local['timetable'])>0){
      $local['status'] = "Cerrado. Horario de hoy {$local['timetable'][$day][$day]}: {$local['timetable'][$day]['open']} a {$local['timetable'][$day]['close']}";
      $opentime = strtotime("$year-$month-$nday {$local['timetable'][$day]['open']}:00");
      $nextday = false;
      $closehour = (integer) $local['timetable'][$day]['close'];
      if( $closehour < 12 ){
        $nnday = $nday+1; $nextday = true;
        $closetime = strtotime("$year-$month-$nnday {$local['timetable'][$day]['close']}:00");
      }else{
        $closetime = strtotime("$year-$month-$nday {$local['timetable'][$day]['close']}:00");
      }

      if($thistime >= $opentime && $thistime <= $closetime){
          if($nextday){
            $dday = ($day+1>6)?0:$day+1;
            $nextday = $local['timetable'][$dday][$dday];
            $local['status']  = "Abierto hasta las {$local['timetable'][$day]['close']} del día $nextday";
          }
          else{
            $local['status']  = "Abierto hasta las {$local['timetable'][$day]['close']}";
          }
          $local['open']    = true;
      }
    }else{
      $local['status'] = "Sin información del horario";
    }
    return ['open'=>$local['open'],'status' => $local['status']];
	}

	public function zoneTime(){
		if(strtolower($this->zone) == 'green')
			return true;
		if(strtolower($this->zone) == 'red')
			return false;

		$year = date('Y');
    $month = date('n');
    $day = date('w');
    $nday = date('j');
    $hour = date('G');
    $minute = date('i');

    $thistime = strtotime("$year-$month-$nday $hour:$minute:00");
    $zonetime = strtotime("$year-$month-$nday 18:00:00");
    return ($thistime<$zonetime);
	}

	public function localCode($uuid){
		if($local = $this->where('uuid',$uuid)->first()){
			return $local->code;
		}
		return false;
	}

	public function localId($uuid){
		if($local = $this->where('uuid',$uuid)->first()){
			return $local->id;
		}
		return false;
	}

}
