<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Nostock extends Model
{
	use SoftDeletes;

    protected $table = 'nostocks';
    protected $dates = ['deleted_at'];

    public function local(){
        return $this->belongsTo('App\Local');
    }

    public function comercio(){
        return $this->belongsTo('App\Comercio');
    }
}
