<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table 			= "images";
    public  	$timestamps = false;

   public function getImages($images,$folder){
   	$server = "http://192.168.11.142/api.papayafast.com/public/images";
    if(is_array($images)){
      if(count($images)>0){
        $images = $this->whereIn('id',$images)->get(['name'])->toArray();
        return collect($images)->flatten()->map(function($item,$key) use ($folder,$server){
        	//return asset("images/$folder/$item");
        	return "$server/$folder/$item";
        })->toArray();
      }else{
      	//return [asset("images/$folder/0.jpg")];
      	return "$server/$folder/0.jpg";
      }
    }
    //return [asset("images/$folder/0.jpg")];
    return "$server/$folder/0.jpg";
  }

}
