<?php

namespace App;

//use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $table = "users";

    protected $casts = ['locals' => 'array'];

    public function address(){
        return $this->hasMany('App\Address');
    }

    /*public function setAuthAttribute($value)
    {
        $this->attributes['auth'] = base64_encode($value);
        //return base64_encode($value);
    }

    public function getAuthAttribute($value)
    {
        //$this->attributes['token'] = base64_decode($value);
        return base64_decode($value);
    }*/
}
