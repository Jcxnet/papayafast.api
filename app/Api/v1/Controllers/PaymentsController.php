<?php

namespace App\Api\v1\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use App\Checkout;
use App\Payment;
use App\Order;

use App\Api\v1\Transformers\DataTransformer;

class PaymentsController extends Controller
{

  /**
   * genera o retorna el id de un Payment para la orden actual
   * @return json
   */
  public function generatePayment(Request $request){
    try{
  		if (! $user = JWTAuth::parseToken()->toUser())
  			return $this->response->errorUnauthorized('Usuario no autorizado');

  		$data = $request->data;
      if(!is_array($data))
      	$data = json_decode($data,true);

      $checkoutid = $this->decryptData($data['payment']);
      $checkout   = new Checkout;
      if(!$checkout->isCheckoutStatus($checkoutid,'PAYMENT')){ //ya creó el método de pago
        return $this->response->item($this->setData(['payment' => $data['payment']]), new DataTransformer)->addMeta('status','ok');
      }
      $data = collect($data)->except('payment')->toArray();
      $paymentid = $this->createPayment($data,$user->id,$checkout->getCheckoutValue($checkoutid,'payment_id'),$request->ip());

     	if($paymentid){
      	$checkout->setCheckoutUpdate($checkoutid,[
      	  	'payment_id' => $paymentid,
      	  	'status' => $checkout->enumStatus('ORDER'),
      	  	'log' => json_encode($this->dataLog($checkout->getCheckoutValue($checkoutid,'log'),'Método de pago generado',$request->ip()))
        ]);
      	return $this->response->item($this->setData(['payment' => $this->encryptData($checkoutid)]), new DataTransformer)->addMeta('status','ok');
      }else{
        return $this->response->error('Ocurrió un error al intentar verificar el método de pago, intente nuevamente',202);
      }
		}catch(JWTException $e){
		  return $this->response->error($e->getMessage(), 202);
		}
  }


  /**
   * genera un nuevo registro Payment y retorna el id, si ya existe retorna el id encontrado
   * @param  array $data
   * @return integer/boolean
   */
  private function createPayment($data,$userid,$paymentid,$ip){
    $payment = [];
    $payment['userid']  = $userid;
    $payment['method']  = strtoupper($data['type']);
    $payment['typecc']  = 'none';
    $payment['expire']  = NULL;
    $payment['tokenpayment']    = NULL;
    $payment['tokenuser']       = NULL;
    $payment['log']             = [];
    $payment['logtransaction']  = [];

    switch(strtolower($payment['method'])){
      case 'cash'   : $payment['number'] ='0';  $payment['log'] = $this->dataLog($payment['log'],"Pago en efectivo agregado, paga con {$data['amount']}",$ip); break;
      case 'pos'    : $payment['number'] ='0';  $payment['log'] = $this->dataLog($payment['log'],"Pago con POS agregado, paga con {$data['card']}",$ip); break;
      case 'credit' : $data['numbercard'] = $this->decryptData($data['numbercard']);
                      $payment['number'] = $data['lastnumbers'];
                      $payment['typecc'] = $data['typecc'];
                      $payment['tokenpayment'] = hash('sha512', env('APP_SALT_KEY').$data['numbercard']);
                      $payment['log'] = $this->dataLog($payment['log'],"Tarjeta agregada por el usuario : {$userid}",$ip);
                      break;
      case 'cclist' : $card = Payment::where('uuid',$data['cardid'])->where('method','CARD')->where('user_id',$userid)->first();
                      if($card){
                        $card->log = $this->dataLog($card->log,"Tarjeta usada en nuevo pago, usuario: $userid",$ip);
                        $card->save();
                        return $card->id;
                      }else{
                        return false;
                      }
      default: return false;
    }
    if($paymentid == 0)
      $newpay = new Payment;
    else
      $newpay = Payment::where('id',$paymentid)->first();

    $newpay->user_id      = $payment['userid'];
    $newpay->number       = $payment['number'];
    $newpay->typecc       = $payment['typecc'];
    $newpay->method       = $newpay->enumMethod($payment['method']);
    //$newpay->expire       = $payment['expire'];
    $newpay->log          = $payment['log'];
    $newpay->tokenuser    = $payment['tokenuser'];
    $newpay->tokenpayment = $payment['tokenpayment'];
    $newpay->save();
    /*if(strtoupper($payment['method']) == 'CREDIT'){
      $data = $this->readSessionKey('paymentDetails');
      $data['cardid'] = $newpay->id;
      $this->saveSessionKey('paymentDetails',$data);
    }*/

    return $newpay->id;
  }

}
