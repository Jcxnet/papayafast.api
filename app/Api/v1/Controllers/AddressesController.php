<?php

namespace App\Api\v1\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\Address;
use App\Local;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator;

use App\Api\v1\Transformers\AddressTransformer;
use App\Api\v1\Transformers\DataTransformer;

/**
 *  # Gestión de Direcciones
 *
 * @Resource ("Addresses", uri="/addresses")
 */
class AddressesController extends Controller
{

   /**
   * Inserta una dirección del usuario en la base de datos
   *
   * @Post("/create")
   * @Versions({"v1"})
   * @Parameters({
 	 *      @Parameter("address", type="string", required=true, description="Dirección exacta"),
 	 *      @Parameter("name", type="string", required=true, description="Nombre que asigna el usuario a la dirección"),
 	 *      @Parameter("phone", type="integer", required=true, description="Número de teléfono asociado a la dirección"),
 	 *      @Parameter("lat", type="float", required=true, description="Valor de la coordenada de Latitud de la dirección"),
 	 *      @Parameter("lng", type="float", required=true, description="Valor de la coordenada de Longitud de la dirección"),
 	 *      @Parameter("local_id", type="integer", required=true, description="Identificador del local que cubre la dirección")
   * })
   * @Transaction({
 	 *      @Request({"address": "dirección", "name": "nombre de la dirección", "phone": "123456", "lat": "-12.4465", "lng":"-72.6545", "local_id":"2"},headers={"Accept":"application/vnd.papayafast.v1+json","Authorization Bearer": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ", "Content-Type":"application/json"}),
   *      @Response(202, body={"meta":{"status": "error"}, "data":{"message":"Usuario no autorizado"}}),
   *      @Response(202, body={"meta":{"status": "error"}, "data":{"message":"No se encontró el local asociado a la dirección"}}),
   *      @Response(202, body={"meta":{"status": "error"}, "data":{"message":"No se pudo crear la dirección"}}),
   *      @Response(202, body={"meta":{"status": "error"}, "data":{"message":"JWTException::class"}}),
   *      @Response(200, body={"meta":{"status": "ok"},"data":{"id":"Address::class->uuid","name":"Address::class->name","address":"Address::class->address","phone":"Address::class->phone","zone":"Address::class->zone","point":{"lat":"Address::class->lat","lng":"Address::class->lng"},"selected":"Address::class->selected","indatabase" :true}})
   * })
   *
   */
  public function create(Request $request){
  	try{
    	if (! $user = JWTAuth::parseToken()->toUser()){
    		return $this->response->errorUnauthorized('Usuario no autorizado');
    	}
    	$data = $this->isValid($request);

      $data['user_id'] = $user->id;
      $data['uuid']		 = $this->generateUuid();

      if(!$local = Local::select('id')->where('uuid',$data['local_id'])->first())
      	return $this->response->error('No se encontró el local asociado a la dirección',202);

      $data['local_id'] = $local->id;
      if($address = Address::create($data)){
      	$address->selected = false;
      	return  $this->response->item($address, new AddressTransformer)->addMeta('status','ok');
      }
      return $this->response->error('No se pudo crear la dirección',202);
    }catch(JWTException $e){
    	return $this->response->error($e->getMessage(), 202);
    }
  }

  /**
   * Busca y retorna la info de una dirección por su uuid
   *
   * @Get("/info")
   * @Versions({"v1"})
   * @Parameters({
 	 *      @Parameter("id", type="string", required=true, description="uuid de la dirección"),
   * })
   * @Transaction({
 	 *      @Request({"id": "6ee6b8f2-cb31-4f9e-a949-edacdeee8ec6"},headers={"Accept":"application/vnd.papayafast.v1+json","Authorization Bearer": "token", "Content-Type":"application/json"}),
   *      @Response(202, body={"error": "Usuario no autorizado"}),
   *      @Response(202, body={"error": "No se encontró la dirección"}),
   *      @Response(202, body={"error": "JWTException::class" }),
   *      @Response(200, body={"meta":{"status": "ok"},"data":{"id":"Address::class->uuid","name":"Address::class->name","address":"Address::class->address","phone":"Address::class->phone","zone":"Address::class->zone","point":{"lat":"Address::class->lat","lng":"Address::class->lng"},"selected":"Address::class->selected","indatabase" :true}})
   * })
   *
   */
  public function getAddress(Request $request){
  	try{
    	if (! $user = JWTAuth::parseToken()->toUser()){
    		return $this->response->errorUnauthorized('Usuario no autorizado');
    	}

    	if($address = Address::where('user_id',$user->id)->where('uuid',$request->id)->get()){
    		return $this->response->item($address, new AddressTransformer)->addMeta('status','ok');
    	}
    	return $this->error('No se encontró la dirección',202);
    }catch(JWTException $e){
    	return $this->response->error($e->getMessage(), 202);
    }
  }

  /**
   * Retorna una colección con las direciones de un usuario
   *
   * @Get("/user")
   * @Versions({"v1"})
   * @Transaction({
 	 *      @Request(headers={"Accept":"application/vnd.papayafast.v1+json","Authorization Bearer": "token", "Content-Type":"application/json"}),
   *      @Response(202, body={"error": "No se encontraron direcciones"}),
   *      @Response(202, body={"error": "JWTException::class" }),
   *      @Response(200, body={"meta":{"status": "ok"},"data":{{"id":"Address::class->uuid","name":"Address::class->name","address":"Address::class->address","phone":"Address::class->phone","zone":"Address::class->zone","point":{"lat":"Address::class->lat","lng":"Address::class->lng"},"selected":"Address::class->selected","indatabase" :true}}})
   * })
   *
   */
   public function myAddresses(Request $request){
  	try{
    	if (! $user = JWTAuth::parseToken()->toUser()){
    		return $this->response->errorUnauthorized('No se encontraron direcciones');
    	}

    	if($address = Address::where('user_id',$user->id)->orderBy('name')->get()){
    		return $this->response->collection($address, new AddressTransformer)->addMeta('status','ok');
    	}
    	return $this->error('No se encontraron direcciones',202);
    }catch(JWTException $e){
    	return $this->response->error($e->getMessage(), 202);
    }
  }

  /**
   * Elimina una dirección del usuario
   *
   * @Delete("/remove")
   * @Versions({"v1"})
   * @Parameters({
 	 *      @Parameter("id", type="string", required=true, description="uuid de la dirección"),
   * })
   * @Transaction({
 	 *      @Request({"id": "6ee6b8f2-cb31-4f9e-a949-edacdeee8ec6"},headers={"Accept":"application/vnd.papayafast.v1+json","Authorization Bearer": "token", "Content-Type":"application/json"}),
   *      @Response(202, body={"error": "Usuario no autorizado"}),
   *      @Response(202, body={"error": "No se encontró la dirección"}),
   *      @Response(202, body={"error": "JWTException::class" }),
   *      @Response(200, body={"meta":{"status": "ok"},"data":{}})
   * })
   *
   */
  public function remove(Request $request){
  	try{
    	if (! $user = JWTAuth::parseToken()->toUser()){
    		return $this->response->errorUnauthorized('Usuario no autorizado');
    	}
    	$uuid = $request->id;
    	if($address = Address::where('uuid',$uuid)->where('user_id',$user->id)->first()){
    		$address->delete();
    		$data = $this->setData([]);
    		return $this->response->item($data,new DataTransformer)->addMeta('status','ok');
    	}else{
    		return $this->response->error('No se encontró la dirección',202);
    	}
    }catch(JWTException $e){
    	return $this->response->error($e->getMessage(), 202);
    }
  }

  /**
   * Actualiza los datos de una dirección del usuario
   *
   * @Put("/update")
   * @Versions({"v1"})
   * @Parameters({
   * 			@Parameter("id", type="string", required=true, description="uuid de la dirección"),
 	 *      @Parameter("address", type="string", required=true, description="Dirección exacta"),
 	 *      @Parameter("name", type="string", required=true, description="Nombre que asigna el usuario a la dirección"),
 	 *      @Parameter("phone", type="integer", required=true, description="Número de teléfono asociado a la dirección"),
 	 *      @Parameter("lat", type="float", required=true, description="Valor de la coordenada de Latitud de la dirección"),
 	 *      @Parameter("lng", type="float", required=true, description="Valor de la coordenada de Longitud de la dirección"),
 	 *      @Parameter("local_id", type="integre", required=true, description="Identificador del local que cubre la dirección")
   * })
   * @Transaction({
 	 *      @Request({"id":"6ee6b8f2-cb31-4f9e-a949-edacdeee8ec6","address": "dirección", "name": "nombre de la dirección", "phone": "123456", "lat": "-12.4465", "lng":"-72.6545", "local_id":"2"},headers={"Accept":"application/vnd.papayafast.v1+json","Authorization Bearer": "token", "Content-Type":"application/json"}),
   *      @Response(202, body={"error": "Usuario no autorizado"}),
   *      @Response(202, body={"error": "No se encontró el local asociado a la dirección"}),
   *      @Response(202, body={"error": "No se encontró la dirección"}),
   *      @Response(202, body={"error": "No se pudo actualizar la dirección"}),
   *      @Response(202, body={"error": "JWTException::class" }),
   *      @Response(200, body={"meta":{"status": "ok"},"data":{"id":"Address::class->uuid","name":"Address::class->name","address":"Address::class->address","phone":"Address::class->phone","zone":"Address::class->zone","point":{"lat":"Address::class->lat","lng":"Address::class->lng"},"selected":"Address::class->selected","indatabase" :true}})
   * })
   *
   */
  public function update(Request $request){
    try{
	   	if (! $user = JWTAuth::parseToken()->toUser()){
	   		return $this->response->errorUnauthorized('Usuario no autorizado');
	   	}
	   	$data = $this->isValid($request);
	   	$uuid = $request->id;
	   	if($address = Address::where('uuid',$uuid)->where('user_id',$user->id)->first()){
	   		if(!$local = Local::select('id')->where('uuid',$data['local_id'])->first())
      		return $this->response->error('No se encontró el local asociado a la dirección',202);
      	$data['local_id'] = $local->id;
      	$data['uuid'] = $uuid;
      	$data['selected'] = isset($request->selected)?$request->selected:false;
    		if($address->update($data))
    			return $this->response->item($address,new AddressTransformer)->addMeta('status','ok');
    		else
    			return $this->response->error('No se pudo actualizar la dirección',202);
    	}else{
    		return $this->response->error('No se encontró la dirección',202);
    	}
	  }catch(JWTException $e){
	   	return $this->response->error($e->getMessage(), 202);
	  }
   }

  /**
   * reglas de validación de los datos de una dirección
   * @param  Request $request
   * @return boolean
   */
  private function isValid(Request $request){
		$data = $request->only(['address', 'name', 'lat', 'lng', 'zone', 'phone','local_id' ]);
    $validator = Validator::make($data, [
       'address'	=> 'required',
       'name'   	=> 'required|string|min:4|max:100',
       'phone'		=> 'required|numeric',
       'lat'  		=> 'required|numeric',
       'lng'  		=> 'required|numeric',
       'zone' 		=> 'required|string',
       'local_id' => 'required',
    ]);
    if($validator->fails()) {
        return $this->response->error(implode(',',$validator->errors()->all()), 202);
    }
    return $data;
  }

}
