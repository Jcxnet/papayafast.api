<?php

namespace App\Api\v1\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\Collection;

use App\Menu;
use App\Api\v1\Transformers\MenuTransformer;
use App\Api\v1\Transformers\DataTransformer;

class MenusController extends Controller
{
  /**
   * lista todos los menús de un comercio, retorna la data de los menús nuevos o actualizados.
   * @param  Request $request
   * @return json
   */
  public function all(Request $request){
  	try{
  		$token 		= $request->apikey;
  		$appMenus = $request->menus;

  		if(!$app = $this->api->post('verify',['apikey'=>$token]))
  			return $this->response->error('No se encontró información para la aplicación',202);
  		$versions = false;
  		if(count($appMenus)>0){
  			$versions = [];
  			foreach ($appMenus as $key => $menu) {
  				$versions[$menu['id']]['version'] = $menu['version'];
  			}
  		}
  		if($menus = Menu::where('comercio_id',$app->comercio_id)->get()){
  			$list = new Collection;
				foreach ($menus as $menu) {
					$menu->page  = $menu->firstPage; // $this->firstMenuPage($menu);
          if($versions){
						if(array_key_exists($menu->uuid,$versions)){
							if($menu->version != $versions[$menu->uuid]['version']){
								$menu = $this->assembleMenu($menu);
                $menu->action = 'update';
							}else{
								$menu->action = 'nochange';
								$menu = $this->removeMenuData($menu);
							}
						}
					}else{
            $menu = $this->assembleMenu($menu);
          }
					$list->push($menu);
				}
				return  $this->response->collection($list, new MenuTransformer)->addMeta('status','update');
  		}
  		return $this->response->error('No se encontraron menús',202);
  	}catch(Exception $e){
  		return $this->response->error($e->getMessage(),202);
  	}
  }

  /**
   * elimina los datos de un menú antes de enciarlos a la app
   * @param  Menu   $menu
   * @return Menu
   */
  protected function removeMenuData(Menu $menu){
  	$menu->menu = [];
  	$menu->page = 0;
  	$menu->pages = [];
  	$menu->buttons = [];
  	$menu->products = [];
  	return $menu;
  }

  /**
   * obtiene la primera página de un menú y genera los botones y productos
   * @param  Menu   $menu
   * @return Page
   */
  protected function assembleMenu(Menu $menu){
  	$menu->pages;
    $menu->buttons;
    $menu->products;
  	return $menu;
  }

}
