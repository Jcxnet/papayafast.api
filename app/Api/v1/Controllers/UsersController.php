<?php

namespace App\Api\v1\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\User;
use App\Appinfo;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use App\Api\v1\Transformers\DataTransformer;
use Validator;

use Image;

class UsersController extends Controller
{

    /**
     * registra un usuario
     * @param  Request $request
     * @return json
     */
    public function register(Request $request){
    	if(!$app = Appinfo::where('token',$request->apikey)->first()){
    		return $this->response->error('La aplicación no está autorizada, intente nuevamente',202);
    	}
    	$result = $this->sendData( env('APIURL_USER_REGISTER'), $request->all() );
	 		if($result['meta']['status'] == 'ok'){

	 			$data = $result['data'];
        $emailData  = [	'urlVerify' => route('user.verify',['token'=>$data['verify_token']]),
        								'name'			=>	$data['name'],
        								'email'			=>	$data['email']];
        $data       = [	'fromName'  => 	env('MAIL_SYSTEM_NAME'),
        								'fromEmail' => 	env('MAIL_SYSTEM_EMAIL'),
        								'toName' 		=> 	$data['name'],
        								'toEmail' 	=> 	$data['email'],
        								'subject' 	=> 	'Bienvenido'];
        $result['data']['emailsent'] = $this->sendEmail(['emails.welcome','emails.welcome-plain'],$emailData, $data);
	 			$data = $this->setData($result['data']);
        return $this->response->item($data, new DataTransformer)->addMeta('status','ok');
	 		}elseif($result['meta']['status'] == 'error'){
	 			return $this->response->error($result['data']['message'], 202);
	 		}else{
	 			return $result;
	 		}
    }

		/**
     * valida el acceso de un usuario
     * @param  Request $request
     * @return json
     */
    public function login(Request $request){
    	if(!$app = Appinfo::where('token',$request->apikey)->first()){
    		return $this->response->error('La aplicación no está autorizada, intente nuevamente',202);
    	}
    	$result = $this->sendData( env('APIURL_USER_LOGIN'), $request->all() );
    	if($result['meta']['status'] == 'ok'){

    		if(!$user = $this->createUser($result['data']['token'],$app->comercio_id))
          return $this->response->error('No se pudo validar el usuario', 202);

      	try {
      		if (! $token = JWTAuth::fromUser($user))
              return $this->response->errorUnauthorized('Usuario no autorizado');
          return $this->response->item( $this->setData(['token' => $token]), new DataTransformer )->addMeta('status','ok');
	      } catch (JWTException $e) {
	          return $this->response->error('could_not_create_token', 202);
	      }
    	}elseif($result['meta']['status'] == 'error'){
	 			return $this->response->error($result['data']['message'], 202);
	 		}else{
	 			return $result;
	 		}
    }

		/**
     * verifica la cuenta de email de un usuario
     * @param  Request $request
     * @return json
     */
    public function verify(Request $request){
    	return $this->response->accepted('Verify account');
    }

    /**
     * retira las credencial de acceso de un usuario
     * @param  Request $request
     * @return json
     */
    public function logout(Request $request){
    	try{
    		if(!$user = JWTAuth::parseToken()->toUser())
    			return $this->response->error('No autorizado',202);

        $result = $this->sendData( env('APIURL_USER_LOGOUT'), [], $this->createTokenHeader($user->auth) );
        if($result['meta']['status']=='ok'){
          JWTAuth::invalidate(JWTAuth::getToken());
          User::unguard();
          $user->update(['auth'=>str_random(60)]);
          User::reguard();
          return $this->response->item($this->setData([]),new DataTransformer)->addMeta('status','ok');
        }
        return $result;
      }catch(JWTException $e){
        return $this->response->error($e->getMessage(), 202);
      }
    }

    /**
     * envía un email con un código de verificación al usuari para poder cambiar su contraseña
     * @param  Request $request
     * @return json
     */
    public function recovery(Request $request){
    	$result = $this->sendData( env('APIURL_USER_RECOVERY'), $request->all() );
      if($result['meta']['status'] == 'ok'){
        $data = $result['data'];
        $dataTemplate = [ 'name'      =>  $data['name'],
                          'code'      =>  $data['code']];
        $dataEmail    = [ 'fromName'  =>  env('MAIL_SYSTEM_NAME'),
                          'fromEmail' =>  env('MAIL_SYSTEM_EMAIL'),
                          'toName'    =>  $data['name'],
                          'toEmail'   =>  $data['email'],
                          'subject'   =>  'Solicitud de cambio de clave'];
        if ($emailsent = $this->sendEmail(['emails.resetpassword','emails.resetpassword-plain'],$dataTemplate, $dataEmail)){
          $data['emailsent'] = true; unset($data['code']);
          $data = $this->setData($data);
          return $this->response->item($data,new DataTransformer)->addMeta('status','ok');
        }
        else
          return $this->response->error('Ocurró un problema al intentar enviar el email, intenta nuevamente',202);
      }elseif($result['meta']['status'] == 'error'){
        return $this->response->error($result['data']['message'], 202);
      }else{
      	return $result;
      }
    }

    /**
     * cambia la contraseña de un usuario
     * @param  Request $request
     * @return json
     */
    public function reset(Request $request){
    	return $this->sendData( env('APIURL_USER_RESET'), $request->all() );
    }

    /**
     * actualiza el nombre y email de un usuario
     * @param  Request $request
     * @return json
     */
    public function update(Request $request){
    	try{
      	if(!$user = JWTAuth::parseToken()->toUser())
    			return $this->response->error('No autorizado',202);
    		$uuid = $user->uuid;
    		$data = $request->only(['name','email']);
    		$file = ($request->hasFile('image'))?$request->file('image'):false;
    		if($file != false){
    			$fileArray = ['image' => $file];
    			$rules = ['image' => 'mimes:jpeg,jpg,png,gif|max:512' // max 512kb
			    				];
			    $validator = Validator::make($fileArray, $rules);
			    if ($validator->fails()){
	          return $this->response->error(implode(',',$validator->errors()->all()), 202);
			    }
			    $imgPath 			= 'images/users'; // upload path
    			$imgExtension = $file->guessClientExtension(); // getting image extension
    			$imgName  		= "$imgPath/$uuid.$imgExtension"; // rename image
    			Image::make($file)->fit(200,200)->save($imgName);
			    $data['image'] = asset($imgName);
    		}

        $result = $this->sendData( env('APIURL_USER_UPDATE'),$data, $this->createTokenHeader($user->auth));
        if ($result['meta']['status']=='ok'){
        	if(is_null($result['data']['image']) || !isset($result['data']['image']))
        		$result['data']['image'] = asset('images/users/0.png');
        	$result['data']['id'] = $uuid;
        }
        return $result;
      }catch(Exception $e){
        return $this->response->error($e->getMessage(),202);
      }
    }

    /**
     * crea un usuario en la base de datos de la API
     * @param  string $uuid_auth
     * @return User
     */
    protected function createUser($token=false,$comercio_id){
    	if(!$token)
        return false;

      $auth_uuid = $this->getKeyToken($token,'sub');
      User::unguard();
      if(! $user = User::where('token',md5($auth_uuid))->first() )
        $user = User::create(['uuid' => $this->generateUuid(), 'token' => md5($auth_uuid), 'auth' => $this->encryptData($token),'comercio_id' =>$comercio_id ]);
      else
      	$user->update(['token' => md5($auth_uuid),'auth' => $this->encryptData($token)]);
      User::reguard();

  		return $user;
    }

    /**
     *  Obtiene información del usuario
     * @param  Request $request
     * @return json
     */
    public function info(Request $request){
    	try{
    		if(!$user = JWTAuth::parseToken()->toUser())
    			return $this->response->error('No autorizado',202);
    		$result  = $this->sendData( env('APIURL_USER_INFO'), [], $this->createTokenHeader($user->auth) );
    		if ($result['meta']['status']=='ok'){
        	$result['data']['id'] = $user->uuid;
        	if(is_null($result['data']['image']) || !isset($result['data']['image'])){
        		$result['data']['image'] = asset('images/users/0.png');
        	}
        	return $result;
        }
        return $result;
      }catch(Exception $e){
        return $this->response->error($e->getMessage(),202);
      }
    }

    public function locals(Request $request){
    	try{
    		if(!$user = JWTAuth::parseToken()->toUser())
    			return $this->response->error('No autorizado',202);
    		$locals = is_null($user->locals)?[]:$user->locals;
    		return $this->response->item($this->setData($locals), new DataTransformer)->addMeta('status','ok');
			}catch(Exception $e){
        return $this->response->error($e->getMessage(),202);
      }
    }

}
