<?php

namespace App\Api\v1\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use App\Order;
use App\Checkout;
use App\Payment;
use App\Appinfo;
use App\Menu;

use App\Api\v1\Transformers\DataTransformer;
use App\Api\v1\Transformers\OrderTransformer;
use App\Api\v1\Transformers\OrderstatusTransformer;

class OrdersController extends Controller
{

	/**
   * crea el pedido y lo almacena en la base de datos
   * @param  Request $request
   * @return json
   */
  public function generateOrder(Request $request){
  	try{
  		if (! $user = JWTAuth::parseToken()->toUser())
  			return $this->response->errorUnauthorized('Usuario no autorizado');

  		$data = $request->data;
      if(!is_array($data))
      	$data = json_decode($data,true);

      if(!$apikey	= $request->apikey)
      	return $this->response->error('La aplicación no tiene acceso, intente nuevamente',202);

      $checkoutid = $this->decryptData($data['payment']);
      $checkout   = new Checkout;
      if(!$checkout->isCheckoutStatus($checkoutid,'ORDER')){ //ya se creó la orden
        return $this->response->item($this->setData(['payment' => $data['payment']]), new DataTransformer)->addMeta('status','ok');
      }

      if(!$payment = Payment::where('user_id',$user->id)->where('id',$checkout->getCheckoutValue($checkoutid,'payment_id'))->where('deleted',false)->first()){
        return $this->response->error('Verifique el método de pago seleccionado',202);
      }

      if(!$products = $data['products']){
      	return $this->response->error('No se encontraron productos en tu pedido',202);
      }

      if(!$address = $data['address']){
      	return $this->response->error('No se encontró la dirección para enviar tu pedido',202);
      }

      if(!$billing = $data['billing']){
      	return $this->response->error('No se encontró la información del método de pago',202);
      }

      if(!$delivery = $data['delivery']){
      	return $this->response->error('No se encontró la información del menú del local',202);
      }

      if(!is_array($products))
      	$products = json_decode($products,true);
      if(!is_array($address))
      	$address = json_decode($address,true);
      if(!is_array($billing))
      	$billing = json_decode($billing,true);

      if(!$products = $this->setProductsPrice($products,$delivery)){
	  		return $this->response->error('No se encontró la información de los precios para su pedido.',202);
	  	}
	  	$data['products'] = $products;

      $order  = $this->createOrder($apikey, $user, $payment->id,$products, $address, $billing,$payment->method);
      if($order['error']){
        return $this->response->error($order['message'],202);
      }
      unset($order['error']);
      unset($order['message']);

      if($payment->method != 'CARD')
        $orderNew = $this->saveOrder(0,$payment->id,$user->id,'PAGADO','GENERADO',$checkout->getCheckoutValue($checkoutid,'order_id'),$data,$request->ip()); //save the order in database
      else
        $orderNew = $this->saveOrder(0,$payment->id,$user->id,'NOPAGADO','GENERADO',$checkout->getCheckoutValue($checkoutid,'order_id'),$data,$request->ip()); //save the order in database

      $orderNew->orderformat = $order; //insert the generated order into order's object
      $orderNew->log    = $this->dataLog($orderNew->log,"Pedido creado",$request->ip());
      $orderNew->save();
      $payment->log = $this->dataLog($payment->log,"Método de pago usado para la orden: {$orderNew->id}",$request->ip());
      $payment->save();
      //$ruc = $this->billingData($billing);

      $checkout->setCheckoutUpdate($checkoutid,[
          'order_id' => $orderNew->id,
          'status' => $checkout->enumStatus('SEND'), //set the next step
          'log' => json_encode($this->dataLog($checkout->getCheckoutValue($checkoutid,'log'),'Pedido generado',$request->ip()))
      ]);
      //next step -> send order to webservice
      $data = ['payment'=> $this->encryptData($checkoutid)];
     	return $this->response->item($this->setData($data), new DataTransformer)->addMeta('status','ok');
  	}catch(JWTException $e){
  		return $this->response->error($e->getMessage(), 202);
    }
  }


 /**
   * almacena en la base de datos el pedido registrado y retorna el ID de la orden creada
   * @param  integer $number
   * @param  integer $paymentid
   * @param  integer $userid
   * @param  string  $payStatus
   * @param  string  $orderStatus
   * @return Order
   */
  private function saveOrder($number, $paymentid, $userid,$payStatus, $orderStatus, $orderid, $data, $ip){
    $products = $data['products'];
    $address  = $data['address'];
    if(!is_array($data['products']))
      	$products = json_decode($data['products'],true);
    $pagos    = $this->calculatePayments($products);
    $payment  = $data['billing'];

    if($orderid == 0)
      $neworder = new Order;
    elseif(!$neworder = Order::where('id',$orderid)->where('orderstatus','!=','ANULADO')->first()){
      $neworder = new Order;
    }
    $neworder->user_id    = $userid;
    $neworder->payment_id = $paymentid;
    $neworder->number     = $number;
    $neworder->created    = date('Y-m-j H:i:s');
    $neworder->amount     = $pagos;
    $neworder->address    = json_decode($address,true);
    $neworder->payment    = json_decode($payment,true);
    $neworder->products   = $products;
    $neworder->paystatus  = $neworder->enumPaymentStatus($payStatus);
    $neworder->orderstatus= $neworder->enumOrderStatus($orderStatus);
    $neworder->subtotal   = $pagos['subtotal'];
    $neworder->comision   = $pagos['comision'];
    $neworder->log        = $this->dataLog($neworder->log,"Pedido creado el {$neworder->created}, usuario: {$userid}, método de pago: {$paymentid}",$ip);

    $neworder->save();
    return $neworder;
  }

  /**
   * crea una orden de acuerdo al formato del negocio
   * @param  User 		$user
   * @param  integer 	$paymentid
   * @param  array 		$products
   * @param  array 		$address
   * @param  array 		$payment
   * @return json
   */
  private function createOrder($apikey, $user, $paymentid,$products, $address, $payment,$method){
  	if(!$processor = $this->createProcessor($apikey)){
  		$data['error'] = true;
      $data['message'] = 'No se pudo crear el procesador para la orden.';
      return $data;
  	}

  	if(count($products)==0){
  		$data['error'] = true;
      $data['message'] = 'Los productos de su pedido no se encontraron en el menú, intente nuevamente.';
      return $data;
  	}

    $pagos = $this->calculatePayments($products);
    if($method == 'CARD'){
    	if($item = Payment::where('id',$paymentid)->first()){
    		$payment['typecc'] = $item->typecc;
    		$payment['lastnumbers'] = $item->number;
    	}
    }

    $user = $this->api->post('users/info',['checksum'=>0]);
  	$user = $user['data'];

    $options = ['user'=>$user,'address'=>$address,'payment'=>$payment,'products'=>$products,'pagos'=>$pagos,'method'=>$method];
    return $processor->generate($options);
  }

  private function setProductsPrice($products,$id){
  	if($menu = Menu::where('uuid',$id)->first()){
  		foreach ($products as $product) {
  			$list[] = $this->getProductPrice($menu->products,$product);
  		}
  		return $list;
  	}else{
  		return false;
  	}
  }

  private function getProductPrice($products,$product){
  	$item = collect($products)->whereIn('code',[$product['code']])->values()->toArray();
  	$product['price'] = 0;
  	if($item){
  		$item = $item[0];
  		$product['price'] 			= $item['price'];
  		$product['properties'] 	= $this->getProductPropertiesPrice($item['properties'],$product['properties']);
  		$product['groups'] 			= $this->getGroupPropertiesPrice($item['groups'],$product['groups']);
  	}
  	return $product;
  }

  private function getProductPropertiesPrice($productProps, $itemProps){
  	$list = [];
  	foreach ($itemProps as $itemProp) {
  		$property = collect($productProps)->whereIn('code',[$itemProp['code']])->values()->toArray();
  		$itemProp['price'] = 0;
  		if($property){
  			$property = $property[0];
  			$itemProp['price'] = $property['price'];
  		}
  		$list[] = $itemProp;
  	}
  	return $list;
  }

  private function getGroupPropertiesPrice($productGroups, $itemGroups){
  	$list = [];
  	foreach ($itemGroups as $itemGroup) {
  		$group = collect($productGroups)->whereIn('id',[$itemGroup['id']])->values()->toArray();
  		if($group){
  			$group = $group[0];
  			$itemGroup['properties'] = $this->getProductPropertiesPrice($group['properties'],$itemGroup['properties']);
  			$list[] = $itemGroup;
  		}
  	}
  	return $list;
  }

  private function createProcessor($apikey){
  	$appinfo = new Appinfo;
  	if(!$processor = $appinfo->getProcessor($apikey,'orders'))
  		return false;

  	try{
  		\App::bind('App\Contracts\Interfaces\OrderInterface', 'App\Contracts\Repository\\'.$processor);
			$processor = \App::make('App\Contracts\Interfaces\OrderInterface');
			return $processor;
  	}catch(Exception $e){
  		return false;
  	}
 }

  /**
   * envía el pedido al local a través del webservice del comercio
   * @return json
   */
  public function sendOrder(Request $request){
		try{
			if (! $user = JWTAuth::parseToken()->toUser())
				return $this->response->errorUnauthorized('Usuario no autorizado');

			$data = $request->data;
	    if(!is_array($data))
	    	$data = json_decode($data,true);

	    if(!$apikey	= $request->apikey)
      	return $this->response->error('La aplicación no tiene acceso, intente nuevamente',202);

	    $checkoutid = $this->decryptData($data['payment']);
	    $checkout   = new Checkout;
	    if(!$checkout->isCheckoutStatus($checkoutid,'SEND')){ //ya se envió la orden
	      $method = strtoupper($checkout->getCheckoutValue($checkoutid,'method'));
        if($method == 'CREDIT' || $method == 'CCLIST')
          return $this->response->item($this->setData(['payment' => $data['payment']]), new DataTransformer)->addMeta('status','payment');
        else
          return $this->response->item($this->setData(['payment' => $data['payment']]), new DataTransformer)->addMeta('status','ok');
	    }

	   	//lock for update -> lockForUpdate()
	   	$order = Order::where('user_id',$user->id)->where('id',$checkout->getCheckoutValue($checkoutid,'order_id'))->where('deleted',false)->lockForUpdate()->first();
      if(!$order){
        return $this->response->item($this->setData(['payment' => $data['payment']]), new DataTransformer)->addMeta('status','notorder');
      }

      $orderNumber = $order->number;
      if($orderNumber == 0){ //generate new order in webservice
      	if(!$processor = $this->createProcessor($apikey)){
					return $this->response->error('No se pudo crear el prorcesador para enviar la orden',202);
				}

	      if(!$result = $processor->send($order->orderformat)){
        	$order->log = $this->dataLog($order->log,"Intento fallido de registro en el servidor del local",$request->ip());
		      $order->save();
		      return $this->response->item($this->setData([]), new DataTransformer)->addMeta('status','nowebservice');
	      }
	      $orderNumber 		= $result;
        $order->number 	= $result;
        $order->log = $this->dataLog($order->log,"Pedido registrado en el servidor del local",$request->ip());
        $order->save();
      }

        $order->orderstatus = $order->enumOrderStatus('PENDIENTE');
        $order->payment = $this->encryptData(json_encode($order->payment));
        $order->save();

        $method = strtoupper($checkout->getCheckoutValue($checkoutid,'method'));
        $statusCheckout  = 'COMPLETED';
        $statusReturn = 'ok';
        if ($method == 'CREDIT' || $method == 'CCLIST'){
          $statusCheckout = 'CREDIT';
          $statusReturn = 'payment';
        }
        $checkout->setCheckoutUpdate($checkoutid,[
            	'status'=> $checkout->enumStatus($statusCheckout),
            	'log' 	=> json_encode($this->dataLog($checkout->getCheckoutValue($checkoutid,'log'),'Pedido confirmado por el servidor del local',$request->ip()))
        ]);
        if($statusReturn=='ok'){
        	$user = $this->api->post('users/info',['checksum'=>0]);
        	$user = $user['data'];
          $data = ['number'=>$orderNumber,'email'=>$this->maskEmail($user['email']) ];
          $order->email = $this->sendEmailOrder($order, $user, date('Y-m-j H:i:s') );
          $order->log   = $this->dataLog($order->log,"Email de confirmación enviado al usuario",$request->ip());
          $order->save();
        }else{
          $data = ['payment'=> $this->encryptData($checkoutid)];
        }
        return $this->response->item($this->setData($data), new DataTransformer)->addMeta('status',$statusReturn);
 	 	}catch(JWTException $e){
  		return $this->response->error($e->getMessage(), 202);
    }
  }


  public function status(Request $request){
  	try{
			if (! $user = JWTAuth::parseToken()->toUser())
				return $this->response->errorUnauthorized('Usuario no autorizado');

			if(!$apikey	= $request->apikey)
	    	return $this->response->error('La aplicación no tiene acceso, intente nuevamente',202);

			if(!$processor = $this->createProcessor($apikey)){
				return $this->response->error('No s epudo crear el prorcesador para enviar la orden',202);
			}

			$uuid = $request->id;
	    // lock for update
	    if($order = Order::where('uuid',$uuid)->where('user_id',$user->id)->where('deleted',false)->lockForUpdate()->first()){
	    	if(!$status = $processor->getstatus(['number'=>$order->number,'local'=>$order->address['local']]))
	    		return $this->response->error('El servicio de pedidos del local se encuentra ocupado, intente nuevamente',202);
	    	$result['changed'] = false;
	    	$result['status']  = $status;
	    	if(strtoupper($status) != $order->orderstatus){
	    		$order->orderstatus = $order->enumOrderStatus($status);
	    		$order->save();
	    		$result['changed'] = true;
	    	}
	    	return $this->response->item($this->setData($result), new DataTransformer)->addMeta('status','ok');
	    }else{
	    	return $this->response->error('No encontramos la orden, intente nuevamente',202);
	    }
		}catch(JWTException $e){
  		return $this->response->error($e->getMessage(), 202);
    }
  }

    /**
   * anula una orden
   * @param  integer  $number
   * @return array
   */
  private function cancelOrder($number,$local){
    $url  = "http://200.37.171.90/wsInforest/ServiceInforest.svc/AnularPedido/$number/$local";
    $ch   = curl_init();

    curl_setopt_array($ch, [
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_POSTFIELDS => "",
		  CURLOPT_HTTPHEADER => ["cache-control: no-cache"],
		]);

    $result = curl_exec($ch);
    curl_close($ch);
    if(!is_array($result))
    	return json_decode($result,true);
    return $result;
  }

  public function confirmation(Request $request){
  	try{
			if (! $user = JWTAuth::parseToken()->toUser())
				return $this->response->errorUnauthorized('Usuario no autorizado');

			if(!$number = $request->number){
				return $this->response->error('No se encontró el número de pedido',202);
			}

      if(!$orderEmail  = Order::where('user_id',$user->id)->where('number',$number)->where('deleted',false)->first()){
        return $this->response->error('El pedido ya no se encuentra disponible',202);
      }
      if(!$user = $this->api->post('users/info',['checksum'=>0])){
	    	return $this->response->error('No se encontró el usuario',202);
	    }
			$user = $user['data'];
	    $emailData =  json_decode($orderEmail->email,true);
      $data = [
    		'fromEmail' =>	env('MAIL_SYSTEM_EMAIL'),
    		'fromName' 	=>	env('MAIL_SYSTEM_NAME'),
    		'toEmail' 	=>	$user['email'],
    		'toName' 		=>	$user['name'],
    		'subject' 	=> 	"Re: Tu pedido {$number} ha sido recibido"
    	];
    	$res = $this->sendEmail(['emails.order','emails.order-plain'],$emailData,$data);
    	$orderEmail->log = $this->dataLog($orderEmail->log,'mensaje de confirmación reenviado',$request->ip());
      $orderEmail->save();
	    return $this->response->item($this->setData(['message'=>'La confirmación de tu pedido fue enviada a '.$this->maskEmail($user['email'])]), new DataTransformer)->addMeta('status','ok');
 	 	}catch(JWTException $e){
  		return $this->response->error($e->getMessage(), 202);
    }
  }

  public function history(Request $request){
   	try{
			if (! $user = JWTAuth::parseToken()->toUser())
				return $this->response->errorUnauthorized('Usuario no autorizado');
			if($orders = Order::where('user_id',$user->id)->where('paystatus','PAGADO')->where('deleted',false)->orderBy('created','desc')->paginate(5)){
    		return $this->response->paginator($orders, new OrderTransformer)->addMeta('status','ok');
    	}else{
    		return $this->response->item($this->setData([]), new DataTransformer)->addMeta('status','ok');
    	}
		}catch(JWTException $e){
		  	return $this->response->error($e->getMessage(), 202);
		}
  }

  public function detail(Request $request){
   	try{
			if (! $user = JWTAuth::parseToken()->toUser())
				return $this->response->errorUnauthorized('Usuario no autorizado');

			if(!$id = $request->id){
				return $this->response->error('No se encontró el número del pedido',202);
			}

			if($order = Order::where('user_id',$user->id)->where('uuid',$id)->where('paystatus','PAGADO')->where('deleted',false)->first()){
    		return $this->response->item($order, new OrderTransformer)->addMeta('status','ok');
    	}else{
    		return $this->response->error('No se encontró el pedido');
    	}
		}catch(JWTException $e){
		  	return $this->response->error($e->getMessage(), 202);
		}
  }

  public function statusByCommerce(Request $request){
  	if(!$apikey	= $request->apikey)
      return $this->response->error('Se necesita el apikey del comercio, intente nuevamente',202);

    if(!$processor = $this->createProcessor($apikey))
    	return $this->response->error('No se pudo crear el procesador para verificar los pedidos, intente nuevamente',202);
    $users 	= $this->usersByCommerce($apikey);
    $status = $this->statusOrdersByCommerce($processor,$users);
    return $this->response->array($status);
  }

  public function statusAllCommerce(Request $request){
  	if(!$commerces = Appinfo::select('comercio_id','token as apikey','name')->get())
  		return $this->response->error('No se encontró ningún cmercio activo',202);

  	$data = [];
  	foreach($commerces as $commerce){
  		$result = '';
  		if(!$processor = $this->createProcessor($commerce->apikey)){
    		$result['data']['message'] 	=	"No se pudo crear el procesador para verificar los pedidos de {$commerce->name}";
    		$result['meta']['status'] 	= 'error';
  		}else{
  			$users = $this->usersByCommerce($commerce->apikey);
    		$result = $this->statusOrdersByCommerce($processor,$users);
    	}
    	$data[] = ['local'=>$commerce->name,'data'=>$result['data'],'status'=>$result['meta']['status']];
  	}

  	return $this->response->array(['data'=>$data,'meta'=>['status'=>'ok']]);
  }

  private function usersByCommerce($apikey){
  	$users = Appinfo::select('users.id','users.comercio_id')->where('apps.token',$apikey)->join('users','users.comercio_id','=','apps.comercio_id')->get();
  	return $users->pluck('id')->toArray();
  }


  private function statusOrdersByCommerce($processor,$users){
  	$status = ['GENERADO','PENDIENTE','LOCAL','PRODUCCION','ENVIADO','NO STATUS'];
  	if($orders = Order::select('uuid','number','orderstatus','address')->whereRaw('number > 0')->whereIn('user_id',$users)->whereIn('orderstatus',$status)->orderBy('created','desc')->get()){
  		$list = []; $total = $orders->count(); $busy = 0; $updated = 0; $nochange = 0;
  		foreach($orders as $order){
  			$result = [];
  			$result['id'] = $order->uuid;
  			$result['number'] = $order->number;
  			if(!$status = $processor->getstatus(['number'=>$order->number,'local'=>$order->address['local']])){
	    		$result['message'] = 'El servicio de pedidos del local se encuentra ocupado, intente nuevamente';
	    		$result['changed'] = false;
	    		$result['status']  = $order->orderstatus;
	    		$busy++;
  			}else{
		    	$result['changed'] = false;
		    	$result['status']  = $status;
		    	$result['message'] = 'El estado del pedido no ha cambiado';
		    	if(strtoupper($status) != $order->orderstatus){
		    		$order->orderstatus = $order->enumOrderStatus($status);
		    		$order->save();
		    		$result['changed'] = true;
		    		$result['message'] = 'El estado del pedido se encuentra actualizado';
		    		$updated++;
		    	}else{
		    		$nochange++;
		    	}
	    	}
	    	$list[] = $result;
  		}
  		$data = ['data'=>['resume'=>['total'=>$total,'fails'=>$busy,'updated'=>$updated,'nochanged'=>$nochange],'orders'=>$list],'meta'=>['status'=>'ok']];
  		return $data;
  	}else{
  		return ['data'=>['message'=>'No se pudo recuperar la lista de pedidos'],'meta'=>['status'=>'error']];
  	}
  }

}

