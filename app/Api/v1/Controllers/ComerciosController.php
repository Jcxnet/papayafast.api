<?php

namespace App\Api\v1\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\Comercio;
use App\Api\v1\Transformers\ComercioTransformer;

class ComerciosController extends Controller
{

    /**
     * extra la información de un comercio
     * @param  Request $request
     * @return json
     */
    public function info(Request $request){
    	try{
    		$uuid	= $request->id;
    		if($comercio = Comercio::where('uuid',$uuid)->first()){
    			return $this->response->item($comercio,new ComercioTransformer)->addMeta('status','ok');
    		}else{
    			return $this->response->error('No se encontró el comercio.',202);
    		}
    	}catch(Exception $e){
        return $this->response->error($e->getMessage(),202);
      }
    }
}

