<?php

namespace App\Api\v1\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use App\Checkout;
use App\Order;
use App\Payment;
use App\Appinfo;

use App\Api\v1\Transformers\CreditcardTransformer;
use App\Api\v1\Transformers\DataTransformer;


class CreditcardsController extends Controller
{

const APP_PAYMENT_SERVICE = 'https://dev-rest.cinepapaya.com/payment/process/one-step-capture';
const APP_PAYMENT_REFUND	= 'https://dev-rest.cinepapaya.com/payment/process/refund';

/**
   * procesa el cobro de la tarjeta de crédito
   * @param  Request $request
   * @return json
   */
  public function process(Request $request){
  	try{
  		if (! $user = JWTAuth::parseToken()->toUser())
  			return $this->response->errorUnauthorized('Usuario no autorizado');

  		if($data = $request->data){
  			if(!is_array($data))
  				$data = json_decode($data, true);
  		}

  		if(!$processor = $this->createProcessor($request->apikey)){
  			return $this->response->error('No se pudo generar el procesador de pagos',202);
	  	}

  		$checkoutid = $this->decryptData($data['payment']);
      $checkout   = new Checkout;
      if(!$checkout->isCheckoutStatus($checkoutid,'CREDIT')){ //ya se realizó el cargo a la tarjeta
        return $this->response->item($this->setData(['payment' => $data['payment']]), new DataTransformer)->addMeta('status','ok');
      }

      if(!$products = $data['products']){
      	return $this->response->error('No se encontraron productos en el pedido',202);
      }
      if(!is_array($products))
      	$products = json_decode($products,true);

      if(!$payment = Payment::where('user_id',$user->id)->where('id',$checkout->getCheckoutValue($checkoutid,'payment_id'))->where('deleted',false)->first()){
        return $this->response->error("Verifique el método de pago seleccionado",202);
      }

      if(!$card = $data['card']){
      	return $this->response->error('No se encontraron los datos de la tarjeta',202);
      }
      if(!is_array($card))
      	$card = json_decode($card,true);

      if(!$method = $data['method']){
      	return $this->response->error('No se encontraron los datos de la tarjeta',202);
      }

      // lock for update
      if( $order = Order::where('id',$checkout->getCheckoutValue($checkoutid,'order_id'))->lockForUpdate()->first() ){

      	$user = $this->api->post('users/info',['checksum'=>0]);
        $user = $user['data'];

      	$errors = [];
        switch (strtolower($method)) {
          case 'credit': $errors = $this->chargeFirstTime  ($user,$card,$order,$products,$payment,$request->ip()); break; // use payment service first time
          case 'cclist': $errors = $this->chargeStoredCard ($user,$order,$products,$request->ip()); break;  // use payment service with stored card
          default: return $this->response->error('El método de pago es incorrecto',202);
        }

        if (count($errors)>0){
        	$errors = json_encode($errors);
        	$order->log  = $this->dataLog($order->log,"Error:$errors",$request->ip());
        	$order->save();
          return $this->response->error($errors,202);
        }else{
        	$checkout->setCheckoutUpdate($checkoutid,[
                'log' => json_encode($this->dataLog(
                            $checkout->getCheckoutValue($checkoutid,'log'),
                            'Cargo generado en la tarjeta',
                            $request->ip())
                				)
                ]);
          $checkout->setCheckoutUpdate($checkoutid,[
                'status' => $checkout->enumStatus('COMPLETED'),
                'log' => json_encode($this->dataLog(
                            $checkout->getCheckoutValue($checkoutid,'log'),
                            'Pedido completado',
                            $request->ip())
                				)
                ]);
          $order->email = $this->sendEmailOrder($order, $user, date('Y-m-j H:i:s') );
          $order->log   = $this->dataLog($order->log,"Email de confirmación enviado al usuario",$request->ip());
          $order->save();
          try{
          	if($processor->setstatus(['number'=>$order->number,'local'=>$order->address['local'],'status'=>2])){ // improve it ¬¬
	          	$order->paystatus  = $order->enumPaymentStatus('PAGADO');
	          	$order->log  = $this->dataLog($order->log,"Se actualizó el estado del pedido a PAGADO",$request->ip());
	          	$order->save();
	          }
	        }catch(Exception $e){
	        	return $this->response->item($this->setData(['number'=>$order->number,'email'=>$this->maskEmail($user['email'])]), new DataTransformer)->addMeta('status','noupdate');
	        }
          return $this->response->item($this->setData(['number'=>$order->number,'email'=>$this->maskEmail($user['email'])]), new DataTransformer)->addMeta('status','ok');
        }
      }else{
      	return $this->response->error('No hemnos podido encontrar tu pedido, intenta nuevamente',202);
      }
		}catch(JWTException $e){
  		return $this->response->error($e->getMessage(), 202);
    }
  }

  private function createProcessor($apikey){
  	$appinfo = new Appinfo;
  	if(!$processor = $appinfo->getProcessor($apikey,'orders'))
  		return false;

  	try{
  		\App::bind('App\Contracts\Interfaces\OrderInterface', 'App\Contracts\Repository\\'.$processor);
			$processor = \App::make('App\Contracts\Interfaces\OrderInterface');
			return $processor;
  	}catch(Exception $e){
  		return false;
  	}
 }

    /**
   * realiza por vez primera el cargo en la tarjeta de crédito del usuario
   * @param  User   $user
   * @param  array  $data
   * @param  Order  $order
   * @return json
   */
  private function chargeFirstTime($user,$data,$order,$products,$payment,$ip){
    $pagos    = $this->calculatePayments($products);

    list($name,$lastName) = explode(' ', $data['nameoncard']);
    list($month,$year) = explode('/',$data['expirecard']);
    $saveCard = ($data['storecard'] == 'true')?true:false;
    $card = [
    'type'          => 'CreditCard',
    'currency'      => 'PEN',
    'country'       => 'PE',
    'amount'        => sprintf("%.2f",$pagos['total']),
    'firstName'     => $name,
    'lastName'      => $lastName,
    'email'         => $user['email'],
    'externalId'    => md5($order->id+time()),
    'forceCurrency' => true,
    'remember'      => $saveCard,
    'number'        => $data['numbercard'],
    'month'         => $month,
    'year'          => $year,
    'cvc'           => $data['securecard'],
    ];

    $result = $this->sendPaymentService(json_encode($card));

    if(is_null($result)){
      return ['El procesador de pagos se encuentra ocupado, no se ha realizado ningún cargo, intenta nuevamente'];
    }

    if(array_key_exists('transactionId',$result)){
      $transactionlog = "transaction done id:{$result['transactionId']} date:{$result['dateTransacion']} currency:{$result['currency']} amount:{$result['amount']}";
      $payment->logtransaction  = $this->dataLog($payment->logtransaction,$transactionlog,$ip);
      $order->log    = $this->dataLog($order->log,$transactionlog,$ip);
      $order->paystatus = $order->enumPaymentStatus('PAGADO');
      $payment->tokenuser = $this->encryptData(json_encode($result));
      if(array_key_exists('dataStored',$result) && $saveCard){
        $payment->tokenuser = $this->encryptData(json_encode([
            'userid'      =>  $result['dataStored']['userId'],
            'storedataid' =>  $result['dataStored']['storedDataId'],
            'nameoncard'  =>  $data['nameoncard'],
            'expirecard'  =>  $data['expirecard'],
        ]));
      }else{
        $payment->deleted = true;
      }

      $order->save();
      $payment->save();

      return [];
    }
    return $this->extractMessages($result);
  }

/**
   * realiza el cargo en una tarjeta de crédito de la lista del usuario
   * @param  User   $user
   * @param  array  $data
   * @param  Order  $order
   * @return json
   */
  private function chargeStoredCard($user,$order,$products,$ip){

    $pagos   = $this->calculatePayments($products);
    $payment = Payment::where('id',$order->payment_id)->first();
    if(!$payment){
      return [['No se encontraron los datos de su tarjeta, no se realizó ningún cargo.']];
    }
    $data = json_decode($this->decryptData($payment->tokenuser), true);
    $cvcnumber = json_decode($this->decryptData($order->payment),true);
    $cvcnumber = $cvcnumber['cvcnumber'];
    list($name,$lastName) = explode(' ', $data['nameoncard']);
    list($month,$year) = explode('/',$data['expirecard']);

    $card = [
    'type'          => 'CreditCard',
    'currency'      => 'PEN',
    'country'       => 'PE',
    'amount'        => sprintf("%.2f",$pagos['total']),
    'firstName'     => $name,
    'lastName'      => $lastName,
    'email'         => $user['email'],
    'externalId'    => md5($order->id+time()),
    'forceCurrency' => true,
    'remember'      => false,
    'number'        => substr($payment->number,-4),
    'month'         => $month,
    'year'          => $year,
    'userId'        => $data['userid'],
    'storedDataId'  => $data['storedataid'],
    'cvc'           => $cvcnumber,
    ];

    $result = $this->sendPaymentService(json_encode($card));

    if(is_null($result)){
      return ['El procesador de pagos se encuentra ocupado, no se ha realizado ningún cargo, intenta nuevamente'];
    }

    if(array_key_exists('transactionId',$result)){
      $transactionlog = "pago realizado id:{$result['transactionId']} date:{$result['dateTransacion']} currency:{$result['currency']} amount:{$result['amount']}";
      $payment->logtransaction  = $this->dataLog($payment->logtransaction,$transactionlog,$ip);
      $order->log    = $this->dataLog($order->log,$transactionlog,$ip);
      $order->paystatus = $order->enumPaymentStatus('PAGADO');

      $order->save();
      $payment->save();

      return [];
    }

    return $this->extractMessages($result);
  }


  public function refundPayment(Request $request){ //not tested
    $user = \Auth::user();
    if(!$user){
      return redirect()->route('home');
    }
    $token = $this->decryptData($request->token);
    $card = ['transactionId' => $token];
    $result = $this->sendPaymentService(json_encode($card),$this->APP_PAYMENT_REFUND);
  }

    /**
   * envía los datos al procesador de pagos
   * @param  json  $payment
   * @return array
   */
  private function sendPaymentService($payment){
    $ch   = curl_init($this::APP_PAYMENT_SERVICE);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, 'Payment_Web_Lentes:RvRjUbV96ZnnokxON07U8dJhsf5CE9gm');
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL,$this::APP_PAYMENT_SERVICE);
    curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-Type: application/json', 'charset=utf-8'));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $payment );
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,0);
		curl_setopt($ch, CURLOPT_TIMEOUT, 60); //timeout in seconds
    $result = curl_exec($ch);
    curl_close($ch);
    return json_decode($result,true);
  }


  public function cards(Request $request){
  	try{
  		if (! $user = JWTAuth::parseToken()->toUser())
  			return $this->response->errorUnauthorized('Usuario no autorizado');

  		if($cards = Payment::where('user_id',$user->id)->where('method','CARD')->where('deleted',false)->get()){
  			return $this->response->collection($cards, new CreditcardTransformer)->addMeta('status','ok');
  		}
  		return $this->response->collection($this->setData([]), new DataTransformer)->addMeta('status','none');
		}catch(JWTException $e){
  		return $this->response->error($e->getMessage(), 202);
    }
  }

  private function extractMessages($list){
  	if(!is_array($list))
  		$list = json_decode($list,true);
  	$messages = [];
  	foreach ($list as $key => $value) {
  		if(is_array($value))
  			$messages[] = $this->extractMessages($value);
  		elseif($key == 'message')
  			$messages[] = $value;
  	}
  	return $messages;
  }

  public function remove(Request $request){
  	try{
  		if (! $user = JWTAuth::parseToken()->toUser())
  			return $this->response->errorUnauthorized('Usuario no autorizado');
  		$cardid = $request->cardid;
  		if($card = Payment::where('user_id',$user->id)->where('method','CARD')->where('uuid',$cardid)->where('deleted',false)->first()){
  			Payment::unguard();
  			$card->update(['deleted'=>true]);
  			Payment::reguard();
  			return $this->response->item($this->setData([]), new DataTransformer)->addMeta('status','ok');
  		}
  		return $this->response->error('No se encontró la tarjeta',202);
		}catch(JWTException $e){
  		return $this->response->error($e->getMessage(), 202);
    }
  }

}

