<?php

namespace App\Api\v1\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use App\Checkout;
use App\Payment;
use App\Order;

use App\Api\v1\Transformers\DataTransformer;

/**
 *  # Validación de los métodos de pago
 *
 * @Resource ("Checkout", uri="/checkouts")
 */
class CheckoutsController extends Controller
{

/**
   * Verifica el método de pago seleccionado por el usuario
   *
   * @Post("/validate")
   * @Versions({"v1"})
   * @Parameters({
 	 *      @Parameter("type", type="string", required=true, description="*cash*:pago en efectivo,*pos*:pago con POS,*credit*:pago con nueva tarjeta de crédito,*cclist*:pago con tarjeta de crédito almacenada"),
 	 *      @Parameter("total", type="float", required=true, description="Total a pagar"),
 	 *      @Parameter("card", type="string", required=true, description="*visa*,*visa electron*,*mastercard* tipo de tarjeta para pago con POS"),
 	 *      @Parameter("inputid", type="string", required=true, description="Identificador del elemento del formulario"),
 	 *      @Parameter("amount", type="float", required=true, description="Monto con el que el usuario cancela el pedido en efectivo"),
 	 *      @Parameter("nameoncard", type="string", required=true, description="Nombre del titular de la tarjeta de crédito"),
 	 *      @Parameter("numbercard", type="integer", required=true, description="Número de la tarjeta de crédito"),
 	 *      @Parameter("expirecard", type="date", required=true, description="Fecha de expiración de la tarjeta MM/YYYY"),
 	 *      @Parameter("securecard", type="integer", required=true, description="Código CCV de la tarjeta de crédito"),
 	 *      @Parameter("billing", type="array", required=true, description="Información de los datos de facturación"),
 	 *      @Parameter("billing['type']", type="string", required=true, description="*boleta*,*factura* Tipo de facturación"),
 	 *      @Parameter("billing['inputid']", type="string", required=true, description="Identificador del elemento del formulario"),
 	 *      @Parameter("billing['name']", type="string", required=true, description="Nombre para la facturación"),
 	 *      @Parameter("billing['ruc']", type="integer", required=true, description="Número de RUC para la factura"),
 	 *      @Parameter("billing['razon']", type="string", required=true, description="Nombre de la empresa para la factura"),
 	 *      @Parameter("billing['address']", type="string", required=true, description="Dirección de la empresa para la factura"),
   * })
   * @Transaction({
 	 *      @Request({"type":"cash","total":"60.00","amount":"100.00","inputid":"txt_total","billing":{"type":"boleta","name":"John Doe","inputid":"txt_boleta"}},headers={"Accept":"application/vnd.papayafast.v1+json","Authorization Bearer": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ","Content-Type":"application/json"}),
 	 *      @Response(202, body={"meta":{"status": "error"}, "data":{"message":"Usuario no autorizado"}}),
 	 *      @Response(202, body={"meta":{"status": "error"}, "data":{"message":"No hemos podido verificar el estado del pedido, intenta nuevamente."}}),
 	 *      @Response(202, body={"meta":{"status": "error"}, "data":{"message":"No se pudo generar el método de validación, intente nuevamente"}}),
	 *      @Response(202, body={"meta":{"status": "error"}, "data":{"errors":{"id":"txt_total","msg":"Solo debe ingresar números"}}}),
	 *      @Response(202, body={"meta":{"status": "error"}, "data":{"errors":{"id":"txt_total","msg":"El monto de pago debe ser mayor al total del pedido"}}}),
	 *      @Response(202, body={"meta":{"status": "error"}, "data":{"message":JWTException::class}}),
 	 *      @Response(200, body={"meta":{"status": "ok"},"data":{"payment":"6affdae3b3c1aa6aa7689e9b6a7b3225a636aa1ac0025f490cca1285ceaf1487"}}),
 	 *
 	 *      @Request({"type":"cash","total":"60.00","amount":"100.00","inputid":"txt_total","billing":{"type":"factura","name":"John Doe","ruc":"12345678901","razon":"Empresa","address":"dirección empresa"}},headers={"Accept":"application/vnd.papayafast.v1+json","Authorization Bearer": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ","Content-Type":"application/json"}),
 	 *      @Response(202, body={"meta":{"status": "error"}, "data":{"message":"Usuario no autorizado"}}),
	 *      @Response(202, body={"meta":{"status": "error"}, "data":{"errors":{"id":"txt_total","msg":"Solo debe ingresar números"}}}),
	 *      @Response(202, body={"meta":{"status": "error"}, "data":{"errors":{"id":"txt_total","msg":"El monto de pago debe ser mayor al total del pedido"}}}),
 	 *      @Response(200, body={"meta":{"status": "ok"},"data":{"payment":"6affdae3b3c1aa6aa7689e9b6a7b3225a636aa1ac0025f490cca1285ceaf1487"}})
   * })
     */
  public function validateMethod(Request $request){
    try{
  		if (! $user = JWTAuth::parseToken()->toUser())
  			return $this->response->errorUnauthorized('Usuario no autorizado');

  		$checkout = new Checkout;
      if($checkout->pendingUserCheckout($user->id)){
        $checkoutid = $checkout->getCheckout($user->id);
      }else{
        $checkoutid = $checkout->generateCheckout($user->id);
      }
      if(!$checkoutid){
      	return $this->response->item($this->setData([]), new DataTransformer)->addMeta('status','invalidtoken')->addMeta('message','No hemos podido verificar el estado del pedido, intenta nuevamente.');
      }

      $data = $request->data;
      if(!is_array($data))
      	$data = json_decode($data,true);

      if(!$checkout->changeCheckoutData($checkoutid,$data)){
        if(!$checkout->isCheckoutStatus($checkoutid,'VALIDATE')){ //ya realizó la verificación
          return $this->response->item($this->setData(['payment' => $this->encryptData($checkoutid)]), new DataTransformer)->addMeta('status','ok');
        }
      }elseif($checkoutid){
      	if(strtolower($data['type']) == 'credit'){ //data was changed but checkout exists->remove checkout,payment and update order!
      		$paymentid = $checkout->getCheckoutValue($checkoutid,'payment_id');
      		$orderid   = $checkout->getCheckoutValue($checkoutid,'order_id');
      		if($orderid != 0){
      			Order::unguard();
	      		Order::where('id',$orderid)->first()->update(['payment_id'=>0]);
	      		Order::reguard();
      		}
      		if($paymentid != 0){
      			Checkout::where('id',$checkoutid)->delete();
	      		Payment::where('id',$paymentid)->delete();
	      		$checkoutid = $checkout->generateCheckout($user->id); //generate new checkout
      		}
      	}
      }

      $type = strtolower($data['type']);
      $resp = [];

      if(!$paymentValidator = $this->createPaymentValidator($type))
      	return $this->response->error('No se pudo generar el método de validación, intente nuevamente',202);

      $data['user'] = $user;
      $resp = $paymentValidator->validate($data);

      if($resp['status'] == 'ok'){
        // store data and step passed
        $checkout->setCheckoutUpdate($checkoutid,[
            'data' => md5(json_encode($data)),
            'status' => $checkout->enumStatus('PAYMENT'),
            'method' => strtoupper($type),
            'log' => json_encode($this->dataLog($checkout->getCheckoutValue($checkoutid,'log'),'Datos validados',$request->ip()))
        ]); //update data value and set next step

        $data['method'] = $type;
        if(strtolower($type) == 'credit'){ //payment with cc, remove data before store
        	unset($data['expirecard']);
          unset($data['securecard']);
          $card = \CreditCard::validCreditCard($data['numbercard']);
          if($card['valid']){
          	$data['typecc'] = $card['type'];
          }
          $data['lastnumbers'] = $this->maskCC($data['numbercard']);
          $data['numbercard'] = $this->encryptData($data['numbercard']);
        }
      	return $this->response->item($this->setData(['payment' => $this->encryptData($checkoutid), 'storage'=>$data]), new DataTransformer)->addMeta('status','storage');
      }
      if($resp['status']=='error'){
      	return $this->response->error(json_encode($resp['errors']),202);
      }
		}catch(JWTException $e){
  		return $this->response->error($e->getMessage(), 202);
    }
  }


  /**
   * Selecciona la interfaz que se utilizará para validar el método de pago seleccionado
   * @param  string $method
   * @return boolean
   */
  private function createPaymentValidator($method){
  	switch(strtolower($method)){
  		case 'cash'		: \App::bind('App\Contracts\Interfaces\PaymentValidatorInterface', 'App\Contracts\Repository\PaymentCash'); 						break;
      case 'pos'    : \App::bind('App\Contracts\Interfaces\PaymentValidatorInterface', 'App\Contracts\Repository\PaymentPos'); 							break;
      case 'credit' : \App::bind('App\Contracts\Interfaces\PaymentValidatorInterface', 'App\Contracts\Repository\PaymentCreditCard'); 			break;
      case 'cclist' : \App::bind('App\Contracts\Interfaces\PaymentValidatorInterface', 'App\Contracts\Repository\PaymentCreditCardStored'); break;
      default: return false;
  	}
		try{
			$paymentValidator = \App::make('App\Contracts\Interfaces\PaymentValidatorInterface');
			return $paymentValidator;
		}catch(Exception $e){
			return false;
		}
 	}


   /**
   * enmascara un número de tarjeta de crédito
   * @param  integer $number
   * @param  integer $count
   * @param  string  $separators
   * @param  string  $mask
   * @return string
   */
  private function maskCC($number, $count = 4, $separators = '-', $mask = '*'){
    $masked = preg_replace('/\d/', $mask, $number);
    $last = preg_match(sprintf('/([%s]?\d){%d}$/', preg_quote($separators),  $count), $number, $matches);
    if ($last) {
        list($clean) = $matches;
        $masked = substr($masked, 0, -strlen($clean)) . $clean;
    }
    return $masked;
  }

  /**
   * Retorna el id del local y la zona de acuerdo a la orden actual
   * @Post("/local")
   * @Versions({"v1"})
   * @Parameters({
 	 *      @Parameter("data", type="json", required=true, description="información encriptada del checkout de la orden actual"),
 	 * })
 	 * @Transaction({
 	 *      @Request({"data":{"payment":"6affdae3b3c1aa6aa7689e9b6a7b3225a636aa1ac0025f490cca1285ceaf1487"}},headers={"Accept":"application/vnd.papayafast.v1+json","Authorization Bearer": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ","Content-Type":"application/json"}),
 	 *      @Response(202, body={"meta":{"status": "error"}, "data":{"message":"Usuario no autorizado"}}),
 	 *      @Response(202, body={"meta":{"status": "error"}, "data":{"message":"No se encontró la orden para enviar el pedido"}}),
	 *      @Response(202, body={"meta":{"status": "error"}, "data":{"message":JWTException::class}}),
 	 *      @Response(200, body={"meta":{"status": "ok"},"data":{"id":"549048e1-52e9-46e9-ba39-3a9a5ca7b076","zone":"green"}}),
 	 *      @Response(200, body={"meta":{"status": "ok"},"data":{"id":"549048e1-52e9-46e9-ba39-3a9a5ca7b076","zone":"orange"}})
 	 * })
   */
  public function local(Request $request){
  	try{
			if (! $user = JWTAuth::parseToken()->toUser())
				return $this->response->errorUnauthorized('Usuario no autorizado');

			$data = $request->data;
	    if(!is_array($data))
	    	$data = json_decode($data,true);

	    $checkoutid = $this->decryptData($data['payment']);
	    $checkout 	= new Checkout;
	    if(!$order 	= Order::where('user_id',$user->id)->where('id',$checkout->getCheckoutValue($checkoutid,'order_id'))->where('deleted',false)->first())
	    	return $this->response->error('No se encontró la orden para enviar el pedido',202);
	    return $this->response->array(['meta'=>['status'=>'ok'],'data'=>['id'=>$order->address['local']['id'],'zone'=>$order->address['zone']]]);
		}catch(JWTException $e){
  		return $this->response->error($e->getMessage(), 202);
    }
  }

}
