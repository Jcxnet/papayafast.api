<?php

namespace App\Api\v1\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Database\Eloquent\Collection;

use App\Nostock;
use App\Local;
use App\Appinfo;

use App\Api\v1\Transformers\DataTransformer;

class NostocksController extends Controller
{

  public function products(Request $request){
  	//comercio info
  	if(!$apikey	= $request->apikey)
      return $this->response->error('La aplicación no tiene acceso, intente nuevamente',202);
    $appinfo = new Appinfo;
  	if(!$comercioid = $appinfo->comercioId($apikey))
  		return $this->response->error('No se encontró el comercio',202);

  	//local info
    if(!$localid = $request->local)
    	return $this->response->error('Se necesita el local',202);
    $local = new Local;
    if(!$localid = $local->localId($localid))
    	return $this->response->error('No se encontró el local',202);

    //products [{code: 123},{code:234}]
    if(!$products = $request->products)
    	return $this->response->error('Se necesita la lista de productos',202);
    if(!is_array($products))
    	$products = json_decode($products, true);

    $items = Nostock::where('comercio_id',$comercioid)->where('local_id',$localid)->get();
    $items = collect($items)->pluck('product_code')->toArray();

    $list = []; $total = 0;
    foreach ($products as $id => $code) {
    	if(in_array($code,$items)){
    		$list[] = $id;
    		$total++;
    	}
    }
    return $this->response->item($this->setData(['unavailables'=>$list, 'total' =>$total]), new DataTransformer)->addMeta('status','ok');
  }

 	public function product(Request $request){
  	//comercio info
  	if(!$apikey	= $request->apikey)
      return $this->response->error('La aplicación no tiene acceso, intente nuevamente',202);
    $appinfo = new Appinfo;
  	if(!$comercioid = $appinfo->comercioId($apikey))
  		return $this->response->error('No se encontró el comercio',202);

  	//local info
    if(!$localid = $request->local)
    	return $this->response->error('Se necesita el local',202);
    $local = new Local;
    if(!$localid = $local->localId($localid))
    	return $this->response->error('No se encontró el local',202);

    //product {123}
    if(!$product = $request->product)
    	return $this->response->error('Se necesita el producto',202);

    $items = Nostock::where('comercio_id',$comercioid)->where('local_id',$localid)->get();
    $items = collect($items)->pluck('product_code')->toArray();

    if(in_array($product,$items))
    	return $this->response->item($this->setData(['available'=>false]), new DataTransformer)->addMeta('status','ok');
    return $this->response->item($this->setData(['available'=>true]), new DataTransformer)->addMeta('status','ok');
  }

}


