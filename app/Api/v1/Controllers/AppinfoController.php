<?php

namespace App\Api\v1\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\Appinfo;
use App\Api\v1\Transformers\AppinfoTransformer;

/**
 *  # Información de la aplicación asociada a cada comercio
 *
 * @Resource ("Appinfo", uri="/")
 */
class AppinfoController extends Controller
{

  /**
   * Busca y compara la versión de la aplicación
   *
   * @Post("/version")
   * @Versions({"v1"})
   * @Parameters({
 	 *      @Parameter("apikey", type="string", required=true, description="clave de la aplicación"),
 	 *      @Parameter("version", type="string", required=true, description="versión de la aplicación enviada para comparar"),
   * })
   * @Transaction({
 	 *      @Request({"apikey": "47cd76e43f74bbc2e1baaf194d07e1fa", "version":"1.0.0"},headers={"Accept":"application/vnd.papayafast.v1+json","Content-Type":"application/json"}),
   *      @Response(202, body={"error": "No se encontró información de la aplicación"}),
   *      @Response(202, body={"error": Exception::class }),
   *      @Response(200, body={"meta":{"status": "update"},"data":{"name":"Appinfo:class->name","version":"Appinfo:class->version","comercio_id":"Appinfo::class->comercio_id"}}),
   *      @Response(200, body={"meta":{"status": "nochange"},"data":{"name":"Appinfo:class->name","version":"Appinfo:class->version","comercio_id":"Appinfo::class->comercio_id"}})
   * })
   *
   */
    public function version(Request $request){
    	try{
    		$token 		= $request->apikey;
    		$version 	= $request->version;
    		if($app = Appinfo::where('token',$token)->first()){
    			if(version_compare($app->version, $version, '>') ){
    				return $this->response->item($app,new AppinfoTransformer)->addMeta('status','update');
    			}
    			return $this->response->item($app,new AppinfoTransformer)->addMeta('status','nochange');
    		}else{
    			return $this->response->error('No se encontró información de la aplicación.',202);
    		}
    	}catch(Exception $e){
        return $this->response->error($e->getMessage(),202);
      }
    }

  /**
   * Busca y retorna la información de la aplicación
   *
   * @Post("/verify")
   * @Versions({"v1"})
   * @Parameters({
 	 *      @Parameter("apikey", type="string", required=true, description="clave de la aplicación"),
   * })
   * @Transaction({
 	 *      @Request({"apikey": "47cd76e43f74bbc2e1baaf194d07e1fa"},headers={"Accept":"application/vnd.papayafast.v1+json","Content-Type":"application/json"}),
   *      @Response(202, body={"error": "No se encontró la aplicación"}),
   *      @Response(202, body={"error": Exception::class }),
   *      @Response(200, body={"meta":{"status": "ok"},"data":{"name":"Appinfo:class->name","version":"Appinfo:class->version","comercio_id":"Appinfo::class->comercio_id"}}),
   * })
   *
   */
    public function getByToken(Request $request){
      try{
        $token  = $request->apikey;
        if($app = Appinfo::where('token',$token)->first()){
          return $this->response->item($app,new AppinfoTransformer)->addMeta('status','ok');
        }
        return $this->response->error('No se encontró la apliación.',202);
      }catch(Exception $e){
        return $this->response->error($e->getMessage(),202);
      }
    }
}

