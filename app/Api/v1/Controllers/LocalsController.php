<?php

namespace App\Api\v1\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\Local;
use App\User;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use App\Api\v1\Transformers\LocalTransformer;
use App\Api\v1\Transformers\LocalcoverTransformer;
use App\Api\v1\Transformers\LocaladdressTransformer;
use App\Api\v1\Transformers\LocalfavoriteTransformer;
use App\Api\v1\Transformers\LocalstatusTransformer;
use App\Api\v1\Transformers\DataTransformer;

use Validator;

class LocalsController extends Controller
{

    /**
     * lista todos los locales de un comercio
     * @param  Request $request
     * @return json
     */
    public function all(Request $request){
    	try{
    		$token = $request->apikey;
    		$checksum = $request->version;

    		if(!$app = $this->api->post('verify',['apikey'=>$token]))
    			return $this->response->error('No se encontró información del comercio',202);

	   		if($app->comercio->localversion == $checksum)
	   			return $this->response->item($this->setData([]), new DataTransformer)->addMeta('status','ok');

    		if($locals = Local::where('comercio_id',$app->comercio_id)->get()){
    				return $this->response->collection($locals, new LocalTransformer)->addMeta('status','update')->addMeta('total',count($locals))->addMeta('version',$app->comercio->localversion);
    		}
    		return $this->response->error('No se encontraron locales',202);
    	}catch(Exception $e){
    		return $this->response->error($e->getMessage(),202);
    	}
    }

    public function coverPoint(Request $request){
  		try{
	  		$validator = Validator::make($request->all(), [
		    	'lat'   => 'required|numeric',
		    	'lng'   => 'required|numeric',
		    	'apikey'=> 'required',
		    ]);
		    if ($validator->fails()) {
		      return $this->response->error(implode(',',$validator->errors()->all()), 202);
		    }

		    $token = $request->apikey;
	  		$lat = $request->lat;
	  		$lng = $request->lng;

	  		if(!$app = $this->api->post('verify',['apikey'=>$token]))
	  			return $this->response->error('No se pudo verificar la dirección',202);

	  		if($locals = Local::where('comercio_id',$app->comercio_id)->get()){
	  			$locals = $this->addressIsCover($locals, $lat, $lng);
	  			if($locals['list']){
	  				$locals = Local::whereIn('id',$locals['locals'])->get();
	  				return $this->response->collection($locals, new LocaladdressTransformer)->addMeta('status','nearest');
	  			}
	  			else
	  				return $this->response->item($locals['local'], new LocalcoverTransformer)->addMeta('status','covered');
	  		}
    		return $this->response->error('No se encontraron locales',202);
    	}catch(Exception $e){
    		return $this->response->error($e->getMessage(),202);
    	}
    }

    private function addressIsCover($locals, $lat, $lng){
    	$point = "$lat $lng";
    	foreach ($locals as $local) {
        foreach($local->zone as $zone){
        	$polygon = $this->getArrayPolygon($zone['point']);
        	if ($this->pointInPolygon($point,$polygon)){
        		$local->cover = $zone['name'];
        		return ['local'=>$local,'list'=>false];
        	}
        }
      }
      // si la dirección no se encuentra en cobertura -> busca locales más cercanos
      $distance = [];
      foreach ($locals as $local)
        $distance [] = ['local' => $local->id, 'distance' => $this->distanceBetweenPoints($lat,$lng,$local->address['point']['lat'],$local->address['point']['lng'] )];

      $sort = [];
      foreach($distance as $k=>$v) {
          $sort['distance'][$k] = $v['distance'];
      }

      array_multisort($sort['distance'], SORT_ASC, $distance); //ordena la lista de locales por distancia

      if (count($distance)>2 )
      	$distance = array_slice($distance,0,2); //retorna los dos locales que se encuentran a menor distancia

      $locals = [];
      foreach($distance as $item)
      	$locals[] = $item['local'];

      return ['list' => true, 'locals'=> $locals];
    }

   /**
   * calcula la distancia entre dos puntos
   * @param  float  $latitudeFrom
   * @param  float  $longitudeFrom
   * @param  float  $latitudeTo
   * @param  float  $longitudeTo
   * @param  integer $earthRadius
   * @return float
   */
  private function distanceBetweenPoints( $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000){
    // convert from degrees to radians
    $latFrom = deg2rad($latitudeFrom);
    $lonFrom = deg2rad($longitudeFrom);
    $latTo = deg2rad($latitudeTo);
    $lonTo = deg2rad($longitudeTo);

    $lonDelta = $lonTo - $lonFrom;
    $a = pow(cos($latTo) * sin($lonDelta), 2) +
      pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
    $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

    $angle = atan2(sqrt($a), $b);
    return $angle * $earthRadius;
  }

    private function getArrayPolygon($array){
      $polygon = [];
      if(count($array)<2)
      	return $polygon;
      for($i=0;$i<count($array)-1; $i+=2){
        $lat = $array[$i];
        $lng = $array[$i+1];
        $point= "$lat $lng";
        $polygon[] = $point;
      }
      return $polygon;
    }

     /**
     * determina si un punto se encuentra dentro de un polígono
     * @param  array  $point
     * @param  array  $polygon
     * @param  boolean $pointOnVertex
     * @return boolean
     */
    private function pointInPolygon($point, $polygon, $pointOnVertex = true) {
        // Transform string coordinates into arrays with x and y values
        $point = $this->pointStringToCoordinates($point);
        $vertices = [];
        foreach ($polygon as $vertex) {
            $vertices[] = $this->pointStringToCoordinates($vertex);
        }

        if ($pointOnVertex == true and $this->pointOnVertex($point, $vertices) == true) {
            return true; //vertice
        }

        // Check if the point is inside the polygon or on the boundary
        $intersections = 0;
        $vertices_count = count($vertices);

        for ($i=1; $i < $vertices_count; $i++) {
            $vertex1 = $vertices[$i-1];
            $vertex2 = $vertices[$i];
            if ($vertex1['y'] == $vertex2['y'] and $vertex1['y'] == $point['y'] and $point['x'] > min($vertex1['x'], $vertex2['x']) and $point['x'] < max($vertex1['x'], $vertex2['x'])) {
                return true; //limite
            }
            if ($point['y'] > min($vertex1['y'], $vertex2['y']) and $point['y'] <= max($vertex1['y'], $vertex2['y']) and $point['x'] <= max($vertex1['x'], $vertex2['x']) and $vertex1['y'] != $vertex2['y']) {
                $xinters = ($point['y'] - $vertex1['y']) * ($vertex2['x'] - $vertex1['x']) / ($vertex2['y'] - $vertex1['y']) + $vertex1['x']; 
                if ($xinters == $point['x']) {
                    return true; //limite
                }
                if ($vertex1['x'] == $vertex2['x'] || $point['x'] <= $xinters) {
                    $intersections++;
                }
            }
        }
        // If the number of edges we passed through is odd, then it's in the polygon.
        if ($intersections % 2 != 0) {
            return true; //inside
        } else {
            return false;
        }
    }

    /**
     * verifica si el punto se encuentra en uno de los vértices del polígono
     * @param  array $point
     * @param  array $vertices
     * @return boolean
     */
    private function pointOnVertex($point, $vertices) {
      foreach($vertices as $vertex) {
        if ($point == $vertex) {
        	return true;
        }
      }
      return false;
    }

    /**
     * convierte un string de coordenada en un array [x->value,y->value]
     * @param  string  $pointString
     * @return array
     */
    private function pointStringToCoordinates($pointString) {
        $coordinates = explode(" ", $pointString);
        return ["x" => $coordinates[0], "y" => $coordinates[1]];
    }

    public function favorite(Request $request){
    	try{
    		if(!$user = JWTAuth::parseToken()->toUser())
    			return $this->response->error('No autorizado',202);
    		$id = isset($request->id)?$request->id:false;
    		if(!$id)
    			return $this->response->error('Se necesita el local', 202);
    		$user->locals = $this->userFavorite($user->locals,$id);
    		$user->save();
    		return $this->response->item($this->setData([]), new DataTransformer)->addMeta('status','ok');
			}catch(Exception $e){
        return $this->response->error($e->getMessage(),202);
      }
    }

    private function userFavorite($list,$id){
    	if(!is_array($list))
    		return [$id];
    	if(in_array($id,$list)){
    		 return collect($list)->diff([$id])->flatten()->toArray();
    	}else{
    		$list[] = $id;
    	}
    	return $list;
    }

    public function userlocals(Request $request){
    	try{
    		if(!$user = JWTAuth::parseToken()->toUser())
    			return $this->response->error('No autorizado',202);
    		$locals = Local::whereIn('uuid',$user->locals)->orderBy('name')->get();
    		return $this->response->collection($locals, new LocalfavoriteTransformer)->addMeta('status','ok');
			}catch(Exception $e){
        return $this->response->error($e->getMessage(),202);
      }
    }

    public function status(Request $request){
    	try{
    		if($local = Local::where('uuid',$request->id)->first()){
    			$local->zone = $request->zone;
    			return $this->response->item($local, new LocalstatusTransformer)->addMeta('status','ok');
    		}else{
    			return $this->response->error('No encontramos el local',202);
    		}
			}catch(Exception $e){
        return $this->response->error($e->getMessage(),202);
      }
    }

    public function open(Request $request){
    	try{
    		if($local = Local::where('uuid',$request->id)->first()->openTime()){
    			return $this->response->item($this->setData(['open'=>$local['open']]), new DataTransformer)->addMeta('status','ok');
    		}else{
    			return $this->response->error('No se pudo determinar el horario del local',202);
    		}
			}catch(Exception $e){
        return $this->response->error($e->getMessage(),202);
      }
    }

    public function delivery(Request $request){
    	try{
    		if($local = Local::where('uuid',$request->id)->first()){
    			return $this->response->item($this->setData(['delivery'=>$local->delivery]), new DataTransformer)->addMeta('status','ok');
    		}else{
    			return $this->response->error('No se pudo determinar el horario del local',202);
    		}
			}catch(Exception $e){
        return $this->response->error($e->getMessage(),202);
      }
    }
}
