<?php

namespace App\Api\v1\Transformers;

use App\Data;
use League\Fractal\TransformerAbstract;

class DataTransformer extends TransformerAbstract{

	public function transform(Data $data){
		return $data->attributesToArray();
	}

}
