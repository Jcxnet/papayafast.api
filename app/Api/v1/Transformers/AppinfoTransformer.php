<?php

namespace App\Api\v1\Transformers;

use App\Appinfo;
use League\Fractal\TransformerAbstract;

class AppinfoTransformer extends TransformerAbstract{

	protected $defaultIncludes = ['comercio'];

	public function transform(Appinfo $app){
		return [
			'name'		 		=> $app->name,
			'version'  		=> $app->version,
			'comercio_id' => $app->comercio_id
		];
	}


	public function includeComercio(Appinfo $app){
    $comercio = $app->comercio;
    return $this->item($comercio, new ComercioTransformer);
  }

}
