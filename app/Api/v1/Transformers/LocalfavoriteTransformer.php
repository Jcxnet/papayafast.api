<?php

namespace App\Api\v1\Transformers;

use App\Local;
use League\Fractal\TransformerAbstract;

class LocalfavoriteTransformer extends TransformerAbstract{

	public function transform(Local $local){
		$isOpen = $local->openTime();
		return [
			'id'	  		=> $local->uuid,
      'name'  		=> $local->name,
      'address'		=> $local->address['address'],
      'point'			=> $local->address['point'],
      'image'			=> $local->images(),
      'timetable' => $local->timetable,
      'open'			=> $isOpen['open'],
      'status'		=> $isOpen['status'],
      'delivery'	=> $local->delivery,
		];
	}

}
