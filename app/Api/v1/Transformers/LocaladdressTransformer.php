<?php

namespace App\Api\v1\Transformers;

use App\Local;
use League\Fractal\TransformerAbstract;

class LocaladdressTransformer extends TransformerAbstract{

	public function transform(Local $local){
		return [
			'id'	  => $local->uuid,
      'name'  => $local->name,
      'address'=> $local->address['address'],
      'point'	=> $local->address['point'],
      'image'			=> $local->images(),
		];
	}

}
