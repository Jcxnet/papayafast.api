<?php

namespace App\Api\v1\Transformers;

use App\Local;
use League\Fractal\TransformerAbstract;

class LocalstatusTransformer extends TransformerAbstract{

	public function transform(Local $local){
		$isOpen = $local->openTime();
		return [
			'id'	  		=> $local->uuid,
      'name'  		=> $local->name,
      'open'			=> $isOpen['open'],
      'status'		=> $isOpen['status'],
      'delivery'	=> $local->delivery,
      'zone'			=> $local->zoneTime(),
		];
	}

}
