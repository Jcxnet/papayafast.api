<?php

namespace App\Api\v1\Transformers;

use App\Order;
use League\Fractal\TransformerAbstract;

class OrderTransformer extends TransformerAbstract{

	public function transform(Order $order){
		return [
			'id'	  		=>	$order->uuid,
			'number' 		=>	$order->number,
			'created' 	=> 	$order->created,
			'subtotal' 	=>	$order->subtotal,
			'comision' 	=>	$order->comision,
			'total'			=> 	$order->subtotal+$order->comision,
			'products'	=>	$order->products,
			'status'		=>	$order->orderstatus,
		];
	}

}
