<?php

namespace App\Api\v1\Transformers;

use App\Local;
use League\Fractal\TransformerAbstract;

class LocalTransformer extends TransformerAbstract{

	public function transform(Local $local){
		$isOpen = $local->openTime();
		return [
			'id'	      => $local->uuid,
			'code'      => $local->code,
      'name'      => $local->name,
      'address'   => $local->address,
      'contact'   => $local->contact,
      'zone'      => $local->zones(),
      'timetable' => $local->timetable,
      'menu'      => $local->menu,
      'image'			=> $local->images(),
      'open'			=> $isOpen['open'],
      'status'		=> $isOpen['status'],
      'delivery'	=> $local->delivery,
		];
	}

}
