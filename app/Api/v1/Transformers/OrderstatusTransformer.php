<?php

namespace App\Api\v1\Transformers;

use App\Order;
use League\Fractal\TransformerAbstract;

class OrderstatusTransformer extends TransformerAbstract{

	public function transform(Order $order){
		return [
			'id'	  		=>	$order->uuid,
			'number' 		=>	$order->number,
			'local' 		=> 	$order->address['local']['id'],
			'status'		=>	$order->orderstatus,
		];
	}

}
