<?php

namespace App\Api\v1\Transformers;

use App\Address;
use League\Fractal\TransformerAbstract;

class AddressTransformer extends TransformerAbstract{

	protected $defaultIncludes = ['local'];

	public function transform(Address $address){
		return [
			'id' 			=> $address->uuid,
			'name'		=> $address->name,
			'address' => $address->address,
			'phone'		=> $this->formatPhone($address->phone),
			'zone'		=> $address->zone,
			'point'		=> ['lat'=> $address->lat,'lng'=> $address->lng],
			'selected' 	=> $address->selected,
			'indatabase' => true,
		];
	}

	public function includeLocal(Address $address){
    return $this->item($address->local, new LocaladdressTransformer);
  }

	protected function formatPhone($phoneNumber) {
    $phoneNumber = preg_replace('/[^0-9]/','',$phoneNumber);

    if(strlen($phoneNumber) > 9) {
        $countryCode = substr($phoneNumber, 0, strlen($phoneNumber)-8);
        $areaCode = substr($phoneNumber, -8, 1);
        $nextThree = substr($phoneNumber, -7, 3);
        $lastFour = substr($phoneNumber, -4, 4);

        $phoneNumber = '+'.$countryCode.' ('.$areaCode.') '.$nextThree.'-'.$lastFour;
    }
    else if(strlen($phoneNumber) == 8) {
        $areaCode = substr($phoneNumber, 0, 1);
        $nextThree = substr($phoneNumber, 1, 3);
        $lastFour = substr($phoneNumber, 4);

        $phoneNumber = '('.$areaCode.') '.$nextThree.'-'.$lastFour;
    }
    else if(strlen($phoneNumber) == 9 && $phoneNumber[0]==9) {
        $nextThree = substr($phoneNumber, 0, 3);
        $nextSix 	 = substr($phoneNumber, 3, 3);
        $lastThree = substr($phoneNumber, 6, 3);

        $phoneNumber = $nextThree.'-'.$nextSix.'-'.$lastThree;
    }
    else if(strlen($phoneNumber) == 9 ) {
        $nextThree = substr($phoneNumber, 0, 3);
        $nextSix 	 = substr($phoneNumber, 3, 3);
        $lastThree = substr($phoneNumber, 6, 3);

        $phoneNumber = '('.$nextThree.') '.$nextSix.'-'.$lastThree;
    }
    else if(strlen($phoneNumber) < 9) {
        $nextThree = substr($phoneNumber, 0, 3);
        $lastNumber = substr($phoneNumber, 3);

        $phoneNumber = $nextThree.'-'.$lastNumber;
    }

    return $phoneNumber;
	}
}
