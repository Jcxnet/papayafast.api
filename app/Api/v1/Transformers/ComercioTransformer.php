<?php

namespace App\Api\v1\Transformers;

use App\Comercio;
use League\Fractal\TransformerAbstract;

class ComercioTransformer extends TransformerAbstract{

	public function transform(Comercio $comercio){
		return [
			'id'			=> $comercio->uuid,
			'name'  	=> $comercio->name,
			'version' => $comercio->localversion
		];
	}

}
