<?php

namespace App\Api\v1\Transformers;

use App\Payment;
use League\Fractal\TransformerAbstract;

class CreditcardTransformer extends TransformerAbstract{

	public function transform(Payment $payment){
		return [
			'id'	  	=> $payment->uuid,
      'number'  => $payment->number,
      'type'		=> $payment->typecc,
		];
	}

}
