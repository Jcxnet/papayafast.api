<?php

namespace App\Api\v1\Transformers;

use App\Local;
use League\Fractal\TransformerAbstract;

class LocalcoverTransformer extends TransformerAbstract{

	public function transform(Local $local){
		return [
			'id'	  => $local->uuid,
      'name'  => $local->name,
      'point'	=> $local->address['point'],
      'zone'	=> $local->cover,
		];
	}

}
