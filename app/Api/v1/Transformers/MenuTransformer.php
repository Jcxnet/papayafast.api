<?php

namespace App\Api\v1\Transformers;

use App\Menu;
use League\Fractal\TransformerAbstract;

class MenuTransformer extends TransformerAbstract{

	public function transform(Menu $menu){
		return [
			'id'	      	=> $menu->uuid,
			'version'			=> $menu->version,
			'type'				=> $menu->type,
			'title'				=> $menu->title,
//			'menu'				=> $menu->menu,
			'page'				=> $menu->page,
			'pages'				=> $menu->pages,
			'buttons'			=> $menu->buttons,
			'products'		=> $menu->products,
			'action'			=> (!isset($menu->action))?'new':$menu->action,
	//		'properties' 	=> $menu->properties,
		];
	}

}
