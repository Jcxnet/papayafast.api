<?php
	namespace App\Contracts\Repository;

	use App\Contracts\Interfaces\PaymentValidatorInterface;
	use App\Contracts\Repository\BillingValidator as Billing;

	class PaymentPos extends Billing implements PaymentValidatorInterface{

		public function validate(array $data){
			return $this->validatePOS($data);
		}

  /**
   * valida los datos del formulario de pago por POS
   * @param  array $data
   * @return array
   */
    private function validatePOS($data){
      $total  = $data['total'];
      $card   = strtolower($data['card']);
      $label  = $data['inputid'];
      $cards  = ['visa','visa electron','mastercard','dinersclub'];
      $errors = $this->validateBilling($data['billing']);
      if(!in_array($card,$cards)){
        $errors[] = ['id'=>$label, 'msg' =>'Parece que el tipo de tarjeta ha cambiado, intente nuevamente!'];
        $error    = ['status' => 'error','errors' => $errors];
        return $error;
      }
      if(count($errors) == 0)
        return ['status' => 'ok'];

      $error = ['status' => 'error','errors' => $errors];
      return $error;
    }

	}