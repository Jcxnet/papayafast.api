<?php
	namespace App\Contracts\Repository;

	use App\Contracts\Interfaces\OrderInterface;
	use App\Local;

	class InforestPardos implements OrderInterface{

		public function generate(array $options){
			$order = '';
			$order = $this->orderHeader($options['address']);
	    $order = $this->orderUserInfo($order,$options['user']);
	    $order = $this->orderAddress($order,$options['address']);
	    $order = $this->orderBilling($order, $options['payment']);
	    $order = $this->orderProducts($order,$options['products']);
	    $order = $this->orderPayment($order,$options['payment'],$options['pagos']);
	    $order['Fechaenvia'] = date('Y-m-j H:i:s');
	    $order['Codigoestadopago'] = ($options['method'] != 'CARD')?2:1; //paid or not
	    return $order;
		}

		public function send($order){
			 if(is_array($order)) //double encoded
	    	$order = json_encode(json_encode($order));
	    else
	    	$order = json_encode($order);
	    return $this->sendOrder($order);
		}

		public function getstatus(array $options){
			$number = $options['number'];
			$data 	= $options['local'];

			$local = new Local;
	    if(!$local = $local->localCode($data['id']))
	    	return false;

			$result = $this->statusOrder($number,$local);
			if(!$result)
				return false;
			return $this->systemStatus($result['Descripcion']);
		}

		public function setstatus(array $options){
			$local = new Local;
	    if(!$local = $local->localCode($options['local']['id']))
	    	return false;
			return $this->updateStatus($options['number'],$local,$options['status']);
		}

		public function cancel(){
			return "Pardos -> cancel";
		}

		private function updateStatus($number,$local,$status){
			// status :  1 = no pagado , 2 =  pagado
	    $url  = "http://200.37.171.90/wsInforest/ServiceInforest.svc/UpdatePedidoEstadoPago/$number/$status/$local";
	    $ch   = curl_init();

	    curl_setopt_array($ch, array(
			  CURLOPT_URL => $url,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			  CURLOPT_POSTFIELDS => "",
			  CURLOPT_HTTPHEADER => ["cache-control: no-cache"],
			));

	    $result = curl_exec($ch);
	    curl_close($ch);
	    $result = json_decode($result,true);

	    if ($result == "Datos actualizados correctamente")
	    	return true;
	    return false;
		}

		private function statusOrder($number, $local){

			$url  = "http://200.37.171.90/wsInforest/ServiceInforest.svc/GetEstadoPedido/$number/$local";
	    $ch   = curl_init();

			curl_setopt_array($ch, [
			  CURLOPT_URL => $url,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			  CURLOPT_POSTFIELDS => "",
			  CURLOPT_HTTPHEADER => ["cache-control: no-cache"],
			]);

	    $result = curl_exec($ch);
	    curl_close($ch);
	    if(!is_array($result))
	    	return json_decode($result,true);
	    return $result;
		}

		private function systemStatus($status){
			switch (strtoupper($status)) {
				case 'PENDIENTE'					: return 'PENDIENTE'; 		break;
				case 'LOCAL ENVIADO'			: return 'LOCAL'; 				break;
				case 'LOCAL RECEPCIONADO'	: return 'PRODUCCION'; 		break;
				case 'LOCAL DESPACHO'			: return 'ENVIADO'; 			break;
				case 'LOCAL ENTREGADO'		: return 'RECIBIDO'; 			break;
				case 'ANULADO'						: return 'ANULADO'; 			break;
				case 'ERROR CONEXION'			: return 'ERROR CONEXION';break;
				case 'DATOS ERRADOS'			: return 'ERROR DATOS'; 	break;
				default 									: return 'NO STATUS';
			}
		}

		private function sendOrder($order){
			$url = 'http://200.37.171.90/wsInforest/ServiceInforest.svc/InsertarPedido';
	    $ch = curl_init($url);

			curl_setopt_array($ch, [
			  CURLOPT_URL => $url,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS => $order,
			  CURLOPT_HTTPHEADER => [
	    	"cache-control: no-cache",
	    	"content-type: application/json"],
			]);

	    $result = curl_exec($ch);
	    curl_close($ch);

	    if(!is_array($result))
	    	$result = json_decode($result,true);

	    if($result == null){
	      return false;
	    }

	    list($msg,$number) = explode(":",$result);
	    $numberOrder = str_replace('"','',trim($number));

	    return $numberOrder;
		}

  /**
   * cabecera de la orden según el formato del local
   * @return array
   */
  private function orderHeader($address){
    $local = new Local;
    if(!$local = $local->localCode($address['local']['id'])){
    	$data['error']      = true;
    	$data['message']    = 'No se encontró el local para la dirección de envío';
    	return $data;
    }
    $data = [];
    $data['Local']      = $local;
    $data['Tipopedido'] = '02';
    $data['error']      = false;
    $data['message']    = '';
    return $data;
  }

  /**
   * información del usuario que genera la orden según el formato del local
   * @param  array  $order
   * @param  User   $user
   * @return array
   */
  private function orderUserInfo($order, $user){
    $data = $order;
    $data['Nombre']   = $user['name'];
    $data['Apellido'] = '-';
    return $data;
  }

  /**
   * dirección donde se enviará la orden según el formato del local
   * @param  array  $order
   * @param  array  $address
   * @return array
   */
  private function orderAddress($order, $address){
    $data = $order;
    if($address){
      $data['Direccion']  = $address['address'];
      $data['Telefono']   = $address['phone'];
    }else{
      $data['error'] = true;
      $data['message'] .= 'Ocurrió un error con la dirección de envío, verifique su lista de direcciones.<br/>';
    }
    return $data;
  }

  /**
   * datos de facturación de la orden según el formato del local
   * @param  array  $order
   * @param  array  $payment
   * @return array
   */
  private function orderBilling($order, $payment){
    $data = $order;
    $billing = $payment['billing'];
    if($billing['type'] == 'boleta'){
      $data['Tipodocumento']= '02';
      $data['Ruc']          = '';
      $data['Razonsocial']  = '';
    }elseif($billing['type'] == 'factura'){
      $data['Tipodocumento']= '01';
      $data['Ruc']          = $billing['ruc'];
      $data['Razonsocial']  = $billing['razon'];
    }else{
      $data['error']    = true;
      $data['message']  .= 'El tipo de comprobante es incorrecto, verifique nuevamente.<br/>';
    }
    return $data;
  }

  /**
   * agrega los productos seleccionados a la orden según el formato del local
   * @param  array  $order
   * @param  array  $products
   * @return array
   */
  private function orderProducts($order, $products){
    $data = $order;
    $items = [];
    $pos = 1;
    foreach ($products as $product) {
      $items[] = $this->orderItem($product,$pos);
      $pos++;
    }
    $data['DetallePedido'] = $items;
    return $data;
  }

  /**
   * agrega un producto a la orden según el formato del local
   * @param  array    $product
   * @param  integer  $pos
   * @return array
   */
  private function orderItem($product,$pos){
    $item = [];
    $item['Item'] = sprintf("%03d",$pos);
    $item['Codigoproducto'] = sprintf("%07d",$product['code']);
    $item['Cantidad'] = $product['cantidad'];
    $item['Observacion']= "";
    if($this->isProductCombo($product)){
      $item['Lcombo'] = "1";
      $item['CPedido'] = $this->orderCombo($product,$pos);
    }else{
      $item['Lcombo'] = "0";
      $item['ProductoPropiedad'] = $this->productProperties($product);
    }
    return $item;
  }

  /**
   * agrega productos tipo combo según el formato del local
   * @param  array    $product
   * @param  integer  $posItem
   * @return array
   */
  private function orderCombo($product,$posItem){
    $props = [];
    $pos   = 1;
    foreach ($product['groups'] as $group) {
      $props = $this->orderSelectedComboProperty($props, $product['code'],$group['properties'],$posItem,$pos);
      $pos++;
    }
    if(count($product['properties'])>0)
      $props = $this->orderAllComboProperties($props, $product['code'],$product['properties'],$posItem,$pos);
    return $props;
  }

 /**
  * agrega las propiedades del producto a la orden según el formato del local
  * @param  array     $props
  * @param  integer   $code
  * @param  array     $properties
  * @param  integer   $posItem
  * @param  integer   $pos
  * @return array
  */
 private function orderAllComboProperties($props, $code, $properties, $posItem,$pos){
    $list = $props;
    foreach ($properties as $property) {
      $prop     = [];
      $prop['Codigoproductocombo']  = sprintf("%07d",$property['code']);
      $prop['Item']                 = sprintf("%03d",$posItem);
      $prop['Codigoproducto']       = sprintf("%07d",$code);
      $prop['Itemcombo']            = sprintf("%03d",$pos);
      $prop['Cantidad']             = $property['cantidad'];
      $prop['Observacion']          = "";
      if(count($prop)>0){
        $list[] = $prop;
        $pos++;
      }
    }
    return $list;
  }


 /**
  * agrega la propiedad seleccionada de cada producto de la orden según el formato del local
  * @param  array     $props
  * @param  integer   $code
  * @param  array     $properties
  * @param  integer   $posItem
  * @param  integer   $pos
  * @return array
  */
 private function orderSelectedComboProperty($props, $code, $properties, $posItem,$pos){
    $list     = $props;
    $prop     = [];
    $selected = NULL;
    $cantidad = 1;
    foreach ($properties as $property) {
      if($property['order'] == 1){
        $selected = $property['code'];
        $cantidad = $property['cantidad'];
        break;
      }
    }
    if($selected != NULL){
      $prop['Codigoproductocombo']  = sprintf("%07d",$selected);
      $prop['Item']                 = sprintf("%03d",$posItem);
      $prop['Codigoproducto']       = sprintf("%07d",$code);
      $prop['Itemcombo']            = sprintf("%03d",$pos);
      $prop['Cantidad']             = $cantidad;
      $prop['Observacion']          = "";
    }
    if(count($prop)>0)
      $list[] = $prop;
    return $list;
  }

  /**
   * agrega las propiedades de cada producto a la orden según el formato del local
   * @param  array  $product
   * @return array
   */
  private function productProperties($product){
    $props = [];
    $pos   = 1;
    foreach ($product['groups'] as $group) {
      $props = $this->orderSelectedProperty($props, $product['code'],$group['properties'],$pos);
      $pos++;
    }
    if(count($product['properties'])>0)
      $props = $this->orderAllProperties($props, $product['code'],$product['properties'],$pos,true);
    return $props;
  }

  /**
   * agrega todas las propiedades del producto según el formato del local
   * @param  array    $props
   * @param  integer  $code
   * @param  array    $properties
   * @param  integer  $pos
   * @return array
   */
  private function orderAllProperties($props,$code, $properties,$pos){
    $list = $props;
    foreach ($properties as $property) {
      $prop = [];
      $prop['Codigoproducto']   = sprintf("%07d",$code);
      $prop['Item']             = sprintf("%03d",$pos);
      $prop['Codigopropiedad']  = sprintf("%04d",$property['code']);;
      $list[] = $prop;
    }
    return $list;
  }

  /**
   * agrega la propiedad seleccionada del producto según el formato del local
   * @param  array    $props
   * @param  integer  $code
   * @param  array    $properties
   * @param  integer  $pos
   * @return array
   */
  private function orderSelectedProperty($props,$code, $properties,$pos){
    $list = $props;
    $prop     = [];
    $selected = NULL;
    foreach ($properties as $property) {
     if($property['order'] == 1){
        $selected = $property['code'];
        break;
      }
    }
    if($selected != NULL){
      $prop['Codigoproducto']   = sprintf("%07d",$code);
      $prop['Item']             = sprintf("%03d",$pos);
      $prop['Codigopropiedad']  = sprintf("%04d",$selected);
    }
    if(count($prop)>0)
      $list[] = $prop;
    return $list;
  }

  /**
   * determina si un producto es combo o no
   * @param  array $product
   * @return boolean
   */
  private function isProductCombo($product){
    //$total = 0;
    $totalG = count($product['groups']);
    $totalP = count($product['properties']);
    //return ($totalP>1 && $totalG>1);
    return ($totalP>0);
  }

  /**
   * agrega los detalles del pago de la orden según el formato del local
   * @param  array    $order
   * @param  array    $payment
   * @param  array    $pagos
   * @param  integer  $userid
   * @return array
   */
  private function orderPayment($order,$payment, $pagos){
    $cards = ['visa'=>'01','mastercard'=>'02','dinersclub' => '04','amex'=>'03','vales'=>'05','cmr'=>'06'];
    $data = $order;

    $pay['Tipopago'] = (strtolower($payment['type']) == "cclist" || strtolower($payment['type']) == "credit")?"02":"01";

    if($pay['Tipopago']=='02'){
      if(array_key_exists('typecc',$payment) && array_key_exists('lastnumbers',$payment)){
        $pay['Tarjeta']  = $this->numberOfCard($payment['typecc']);
        $pay['Numero']   = substr($payment['lastnumbers'],-4);
        $pay['Vuelto']   = "0.00";
      }else{
        $data['error'] = true;
        $data['message'] .= 'El tipo de tarjeta es desconocido.<br/>';
      }
    }else{
      $pay['Numero'] = "";
      $pay['Tarjeta'] = "";
      if((strtolower($payment['type']) == "cash")){
        $paga = ceil($payment['amount']);
        $monto = $pagos['total'];
        $pay['Vuelto'] = sprintf("%01.2f",round($paga-$monto,1));
        $pay['Observacion'] = 'Paga en efectivo con S/. '.sprintf("%01.2f",$paga);
      }elseif(strtolower($payment['type']) == "pos"){
        $pay['Vuelto'] = "0.00";
        $pay['Observacion'] = 'Paga con POS '.$payment['card'];
      }else{
        $data['error'] = true;
        $data['message'] .= 'El método de pago ha cambiado, verifique su pedido.<br/>';
      }
    }
    $pay['Monto'] = sprintf("%01.2f",$pagos['total']);
    $data['Prepagos'] = $pay;
    return $data;
  }

  private function numberOfCard($type){
  	switch(strtolower($type)){
  		case 'visa'				: return '01'; break;
  		case 'mastercard'	: return '02'; break;
  		case 'dinersclub'	: return '04'; break;
  		case 'amex'				: return '03'; break;
  		case 'vales'			: return '05'; break;
  		case 'cmr'				: return '06'; break;
  		default 					: return '01';
  	}
  }

	}