<?php
	namespace App\Contracts\Repository;

	use App\Contracts\Interfaces\PaymentValidatorInterface;
	use App\Contracts\Repository\BillingValidator as Billing;

	use App\Payment;

	class PaymentCreditCardStored extends Billing implements PaymentValidatorInterface{

		public function validate(array $data){
			return $this->validateCreditCardStored($data);
		}

   	private function validateCreditCardStored($data){
      $label  = $data['inputid'];
      if(!array_key_exists('cardid',$data)){
        $errors[] = ['id'=>$label, 'msg' =>'No hemos encontrado la tarjeta seleccionada!'];
        $error = ['status' => 'error','errors' => $errors];
        return $error;
      }
      $cardid = $data['cardid'];
      $label  = $data['inputid'];
      $cvcnumber = $data['cvcnumber'];

      $errors = $this->validateBilling($data['billing']);
      if(count($errors) > 0)
        return ['status' => 'error','errors' => $errors];

      $errors = [];
      if($cvcnumber == '' || trim($cvcnumber) == '' || strlen($cvcnumber)<3)
         $errors[] = 'Verifique el código de verificación de la tarjeta.';
      elseif(!is_numeric($cvcnumber))
        $errors[] = 'Solo ingrese números en el código de verificación.';

      if($card = Payment::where('uuid',$cardid)->where('method','CARD')->where('user_id',$data['user']->id)->where('deleted',false)->first()){
      	$expire = json_decode($this->decryptData($card->tokenuser),true);
      	$expire = $expire['expirecard'];
        if(!$this->checkDateExpiration($expire)){
          $errors[] = 'Parece que la tarjeta seleccionada ya expiró.';
          $card->log = $this->dataLog($card->log,"Intento de uso de tarjeta de crédito vencida, card_id: {$card->id}");
          $card->save();
        }
        if($card->deleted){
          $errors[] = 'La tarjeta seleccionada ya no se encuentra disponible.';
          $card->log = $this->dataLog($card->log,"Intento de uso de tarjeta de crédito eliminada, card_id: {$card->id}");
          $card->save();
        }
      }else{
        $errors[] = 'No hemos encontrado la tarjeta seleccionada!';
      }

      if(count($errors) == 0)
        return ['status' => 'ok'];
      else
        return ['status' => 'invalidtoken','message' => implode('<br/>',$errors)];
    }

    private function checkDateExpiration($data) {
      list($mm,$yy) = explode('/',$data);
      $nums = strlen($yy);
      if(date('Y') > $yy)
        return false;
      if(date('n')> $mm)
        return false;
      return true;
    }

	}