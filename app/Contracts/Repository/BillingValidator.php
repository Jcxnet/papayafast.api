<?php
	namespace App\Contracts\Repository;

	class BillingValidator{
		 /**
     * valida el tipo de comprobante
     * @param  array $data
     * @return array
     */
    public function validateBilling($data){
      if($data['type'] == 'boleta')
        return $this->validateBoleta($data);
      if($data['type'] == 'factura')
        return $this->validateFactura($data);
      return [];
    }

    /**
     * valida los datos del comprobante tipo factura
     * @param  array $data
     * @return array
     */
    private function validateFactura($data){
      $ruc      = $data['ruc'];
      $razon    = $data['razon'];
      $address  = $data['address'];
      $labels   = $data['inputid'];
      $errors   = [];
      if($razon == '' || trim($razon) == '' || strlen($razon)<2)
         $errors[] = ['id'=>'#razon', 'msg' =>'La Razón social ingresada para la factura es incorrecta.'];
      if($address == '' || trim($address) == '' || strlen($address)<2)
         $errors[] = ['id'=>'#address', 'msg' =>'La Dirección fiscal ingresada para la factura es incorrecta.'];
      if($ruc == '' || trim($ruc) == '' || strlen($ruc)<11)
        $errors[] = ['id'=>'#ruc', 'msg' =>'Debe ingresar el número de RUC completo (11 dígitos).'];
      elseif(!$this->validateRUC($ruc))
         $errors[] = ['id'=>'#ruc', 'msg' =>'El RUC ingresado para la factura es incorrecto.'];
      return $errors;
    }

    /**
     * verifica si un RUC es válido
     * @param  string $ruc
     * @return boolean
     */
    private function validateRUC($ruc){
      if(!(strlen($ruc)==11))
        return false;
      $numbers  = str_split($ruc);
      $last     = $numbers[count($numbers)-1];
      unset($numbers[count($numbers)-1]);
      if(!(count($numbers) == 10))
        return false;
      $values = [5, 4, 3, 2, 7, 6, 5, 4, 3, 2];
      $sumaproducto = 0;
      for($i=0;$i<10; $i++){
        if(!is_numeric($numbers[$i]))
          return false;
        $sumaproducto += ($values[$i] * $numbers[$i]);
      }
      $div = (int)($sumaproducto/11);
      return ($last == ( 11 - ($sumaproducto - $div * 11) ) );
    }

    /**
     * valida los datos del comporobante tipo boleta
     * @param  array  $data
     * @return array
     */
    private function validateBoleta($data){
      $name  = $data['name'];
      $label = $data['inputid'];
      $error = [];
      if($name == '' || trim($name) == '' || strlen($name)<2){
        $error[] = ['id'=>$label, 'msg' =>'El nombre ingresado para la boleta es incorrecto.'];
      }
      return $error;
    }
	}