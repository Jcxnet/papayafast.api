<?php
	namespace App\Contracts\Repository;

	use App\Contracts\Interfaces\PaymentValidatorInterface;
	use App\Contracts\Repository\BillingValidator as Billing;

	class PaymentCash extends Billing implements PaymentValidatorInterface{

		public function validate(array $data){
			return $this->validateCash($data);
		}

    /**
     * valida los datos del formulario de pago en efectivo
     * @param  array $data
     * @return array
     */
    private function validateCash($data){
      $total  = $data['total'];
      $amount = $data['amount'];
      $label  = $data['inputid'];
      $errors = $this->validateBilling($data['billing']);
      if(is_numeric($amount)){
        if($amount < $total){
          $errors[] = ['id'=>$label, 'msg' =>'El monto de pago debe ser mayor al total del pedido'];
          $error    = ['status' => 'error','errors' => $errors];
          return $error;
        }
      }else{
        $error = ['status' => 'error'];
        $errors[] = ['id'=>$label, 'msg' =>'Solo debe ingresar números'];
        $error['errors'] = $errors;
        return $error;
      }
      if(count($errors) == 0)
        return ['status' => 'ok'];

      $error = ['status' => 'error','errors' => $errors];
      return $error;
    }

	}