<?php
	namespace App\Contracts\Repository;

	use App\Contracts\Interfaces\PaymentValidatorInterface;
	use App\Contracts\Repository\BillingValidator as Billing;

	use App\Payment;

	class PaymentCreditCard extends Billing implements PaymentValidatorInterface{

		public function validate(array $data){
			return $this->validateCreditCard($data);
		}

    /**
     * valida los datos del formulario de pago con Tarjeta de Crédito
     * @param  array $data
     * @return array
     */
    private function validateCreditCard($data){
      $nameoncard = $data['nameoncard'];
      $numbercard = $data['numbercard'];
      $expirecard = $data['expirecard'];
      $securecard = $data['securecard'];
      $labels     = $data['inputid'];
      $errors = $this->validateBilling($data['billing']);

      if($nameoncard == '' || trim($nameoncard) == '' || strlen($nameoncard)<5)
         $errors[] = ['id'=>'#nameoncard', 'msg' =>'Verifique el nombre que aparece en la tarjeta.'];
      if($numbercard == '' || trim($numbercard) == '' || strlen($numbercard)<12)
         $errors[] = ['id'=>'#numbercard', 'msg' =>'Verifique el número de la tarjeta.'];
      elseif(!is_numeric($numbercard))
        $errors[] = ['id'=>'#numbercard', 'msg' =>'Solo ingrese números.'];
      else{
        $card = \CreditCard::validCreditCard($numbercard);
        if(!$card['valid'])
          $errors[] = ['id'=>'#numbercard', 'msg' =>'Este número de tarjeta es incorrecto, intente con otro.'];
        else{
          $card = Payment::where('method','CARD')->where('tokenpayment',hash('sha512', env('APP_SALT_KEY').$numbercard))->where('deleted',false)->first();
          if($card){
            $errors[] = ['id'=>'#numbercard', 'msg' =>'Esta tarjeta ya se encuentra registrada!'];
          }
        }
      }
      if($expirecard == '' || trim($expirecard) == '' || strlen($expirecard)<6)
         $errors[] = ['id'=>'#expirecard', 'msg' =>'Verifique la fecha de expiración de la tarjeta.'];
      elseif(!$this->checkDateFormat($expirecard))
        $errors[] = ['id'=>'#expirecard', 'msg' =>'Ingrese el Mes/Año de expiración de la tarjeta.'];
      else{
        list($mm,$yy) = explode('/',$expirecard);
        $validDate = \CreditCard::validDate($yy, $mm);
        if(!$validDate)
          $errors[] = ['id'=>'#expirecard', 'msg' =>'Parece que la tarjeta ya expiró.'];
      }
      if($securecard == '' || trim($securecard) == '' || strlen($securecard)<3)
         $errors[] = ['id'=>'#securecard', 'msg' =>'Verifique el código de verificación de la tarjeta.'];
      elseif(!is_numeric($securecard))
        $errors[] = ['id'=>'#securecard', 'msg' =>'Solo ingrese números.'];
      else{
        $card = \CreditCard::validCreditCard($numbercard);
        $validCvc = \CreditCard::validCvc($securecard, $card['type']);
        if(!$validCvc)
          $errors[] = ['id'=>'#securecard', 'msg' =>'Verifique este número en su tarjeta!'];
      }
      if(count($errors) == 0)
        return ['status' => 'ok'];

      $error = ['status' => 'error','errors' => $errors];
      return $error;
    }

    private function checkDateFormat($data) {
      if(strpos($data,'/') === false)
        return false;
      if(substr_count($data,'/')>1)
        return false;
      list($mm,$yy) = explode('/',$data);
      if(!(is_numeric($mm)) || !(is_numeric($yy)))
        return false;
      if ( checkdate($mm,1,$yy) ){
        return true;
      }else{
        return false;
      }
    }

	}