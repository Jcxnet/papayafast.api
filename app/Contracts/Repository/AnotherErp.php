<?php
	namespace App\Contracts\Repository;

	use App\Contracts\Interfaces\OrderInterface;

	class AnotherErp implements OrderInterface{
		public function generate(array $options){
			return "Otro erp -> generate";
		}
		public function send($order){
			return "Otro erp -> send";
		}
		public function getstatus(array $options){
			return "Otro erp -> get status";
		}
		public function setstatus(){
			return "Otro erp -> set status";
		}
		public function cancel(){
			return "Otro erp -> cancel";
		}
	}