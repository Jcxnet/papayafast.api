<?php
	namespace App\Contracts\Interfaces;

	interface PaymentValidatorInterface{

		public function validate(array $data);

	}