<?php
	namespace App\Contracts\Interfaces;

	interface OrderInterface{

		public function generate(array $options);
		public function send($order);
		public function getstatus(array $options);
		public function setstatus(array $options);
		public function cancel();

	}